=pod
Author: Michelle Ho
Compute Levenstein scores for each combination of models (pair-wise between genes?)
Look into Clustal for multiple alignment


Levenshtein distances
    Cost: cost = -5 if gene found in both sets, else cost = +1

"outliers" refer to models with no common genes to model of reference.
=cut

use List::Util qw(min);
#use Algorithm::Combinatorics qw(combinations);
use warnings;
use strict;

my $model_file = "sortedModels_topotecan.tsv";
#$model_file = "test_levenshteinDistances.txt";
my @gene_list;
my @genes_levenshtein;

=pod
#for original sortedModels.tsv
open OUTMODEL, "<$model_file" or die $!;
<OUTMODEL>; #Skip header
while (<OUTMODEL>){
    chomp;
    my @columns = split("\t", $_);
    my $genes = $columns[1];
    #$genes =~ tr/;/ /;
    my @g = split(";", $genes);
    @g = sort {$a cmp $b} @g;
    $genes = join ' ', @g;
    #print("$genes\n");
    #my @gene_combinations = getCombinations($columns[2]);

    
    push @gene_list, $genes;
}
close OUTMODEL;
=cut

=pod
#for test gene set
open MODEL, "<$model_file" or die $!;
while(<MODEL>){
    chomp;
    my @g = split(";", $_);
    @g = sort {$a cmp $b} @g;
    my $genes = join ' ', @g;
    push @gene_list, $genes;
}

close MODEL;
=cut

#from sortedModels.tsv
#$model_file = "sortedModels.tsv";
open MODEL, "<$model_file" or die $!;
<MODEL>; #skip first line
while(<MODEL>){
    chomp;
    my @columns = split("\t", $_);
    my @g = split(";", $columns[3]); #column with gene signature
    @g = sort {$a cmp $b} @g;
    my $genes = join ' ', @g;
    push @gene_list, $genes;
}

close MODEL;

#get rid of repeat gene sets
my %gene_params = map { $_ => 1 } @gene_list;
@gene_list = keys %gene_params;
@gene_list = sort {$a cmp $b} @gene_list;


#get pairwise combinations
my $range = @gene_list;
my $continue = 0;
my @comb;
my $j;
my $i = 0;
while ($i < $range - 1){
    $j = $i + 1;
    while($j < $range){
        if($gene_list[$i] ne $gene_list[$j]) {
            my $combination = "$gene_list[$i]\t$gene_list[$j]";
            push @comb, $combination;
        }
        $j++;
    }
    $i ++;
}

my $lenght = @comb;
print ("COMBINING ".$lenght."\n");

my @array_with_scores;
open OUTFILE, ">models_levensteinDistances.tsv" or die $!; #signature A /t Signature B /t Levenshtein Distances
print OUTFILE "SignatureA\tSignatureB\tLD\n";

foreach my $combination (@comb){
    my @geneset = split("\t", $combination);
    my $gene_to_test_1 = $geneset[0];
    my $gene_to_test_2 = $geneset[1];

    $gene_to_test_1 =~ s/^\s+|\s+$//;
    $gene_to_test_2 =~ s/^\s+|\s+$//;

    my $test_leven = levenshtein($gene_to_test_1, $gene_to_test_2);
    print("$gene_to_test_1\t||\t$gene_to_test_2\t||\t$test_leven\n");
    print OUTFILE "$gene_to_test_1\t$gene_to_test_2\t$test_leven\n";

    my $addtoarray = "$gene_to_test_1\t$gene_to_test_2\t$test_leven";
    push @array_with_scores, $addtoarray;
}
close OUTFILE;

open OUTFILE, ">models_levensteinDistances_organized.txt" or die $!;
open OUTTSV, ">models_levensteinDistances_tabs.tsv" or die $!;
print OUTTSV "Signature\tNumber_unique\tNumber_similar\tSimilar_signatures\tLowest_LD\tLowest_LD_signature\n";
foreach my $geneset (@gene_list){
    my @gs = split(" ", $geneset);
    my $length_geneset  = @gs;
    my @outliers;
    my @lowestScore = (100, "");
    my $nonOutliers = 0;
    my @nonOutliers_arr;

    foreach (@array_with_scores){
        my @columns = split("\t", $_);
        my $s_geneset = $columns[0];
        my $s_genesetComp = $columns[1];
        my $s_leven = $columns[2];


        
        if ($s_geneset eq $geneset || $s_genesetComp eq $geneset){

            my @s_gs = split(" ", $s_genesetComp);
            if ($s_genesetComp eq $geneset){
                @s_gs = split(" ", $s_geneset)
            }
            my $length_s = @s_gs;

            if ((($length_geneset >= $length_s) && ($s_leven == $length_geneset)) || ((($length_s >= $length_geneset) && ($s_leven == $length_s)))){
                if ($s_geneset eq $geneset){
                    push @outliers, $s_genesetComp;
                }
                else {
                    push @outliers, $s_geneset;
                }
               

            } 

            else{

                $nonOutliers ++;

                if ( $s_genesetComp eq $geneset){
                    push @nonOutliers_arr, $s_geneset.", ".$s_leven;

                    if ($s_leven < $lowestScore[0]){
                        $lowestScore[0] = $s_leven;
                        $lowestScore[1] = $s_geneset;
                    }

                } else {
                    push @nonOutliers_arr, $s_genesetComp.", ".$s_leven;

                    if ($s_leven < $lowestScore[0]){
                        $lowestScore[0] = $s_leven;
                        $lowestScore[1] = $s_genesetComp;
                    }

                }

            }

        }
    }

    my $length_outliers = @outliers;

    print ("\nSIGNATURE: $geneset\n");
    #print join ("; ", @outliers);
    print "Number of signatures unique to this signature: $length_outliers \nSimilar: $nonOutliers\n";
    print join ("\n", @nonOutliers_arr);
    print "\nShortest levenshtein distance: $lowestScore[1] at distance $lowestScore[0]\n";

    print OUTFILE "SIGNATURE: $geneset\n";
    #print OUTFILE join ("; ", @outliers);
    print OUTFILE "Number of signatures unique to this signature: $length_outliers \nSimilar: $nonOutliers\n";
    print OUTFILE join ("\n", @nonOutliers_arr);
    print OUTFILE "\nShortest levenshtein distance: $lowestScore[1] at distance $lowestScore[0]\n\n";

    print OUTTSV "$geneset\t$length_outliers\t$nonOutliers\t";
    print OUTTSV join (";", @nonOutliers_arr);
    print OUTTSV "\t$lowestScore[0]\t$lowestScore[1]\n";
}
close OUTFILE;
close OUTTSV;


sub levenshtein {
    my ($str1, $str2) = @_;
    my @ar1 = split / /, $str1; #/" "/ to split per word rather than per letter
    my @ar2 = split / /, $str2;

    my @dist;
    $dist[$_][0] = $_ foreach (0 .. @ar1);
    $dist[0][$_] = $_ foreach (0 .. @ar2);

    foreach my $i (1 .. @ar1){
        foreach my $j (1 .. @ar2){
            my $cost = $ar1[$i - 1] eq $ar2[$j - 1] ? -5 : 1; #cost = -5 instead of 0 if a gene occurs in both sets to give preference
            $dist[$i][$j] = min(
                        $dist[$i - 1][$j] + 1, 
                        $dist[$i][$j - 1] + 1, 
                        $dist[$i - 1][$j - 1] + $cost );
        }
    }

    return $dist[@ar1][@ar2];
}

=pod
sub getCombinations {
    my($gene) = @_;
    
    my @geneList = split(";", $gene);
    my $length = $geneList;
    my @gene_combinations;

    
}


sub combine {

  my ($list, $n) = @_;
  #die "Insufficient list members" if $n > $list;
  
  $list = $list[0];
  print("LIST\n");
  print($list);
  if ($n <=1){
      return map ([$_], @$list);
  }

  my @comb;

  for (my $i = 0; $i+$n <= @$list; ++$i) {
    my $val  = $list->[$i];
    my @rest = @$list[$i+1..$#$list];
    push @comb, [$val, @$_] for combine (\@rest, $n-1);
  }

  return @comb;
}
=cut
