use warnings;
use strict;

my($boolean_svm, $boolean_modelBuilding, $boolean_probeFinding, $boolean_modelTesting, $labelFile, $step0, $step1, $step2) = @ARGV;

#pathway to directories for steps 1 through 5
my $path_MFA = "1.PerformingMFA";
my $path_SVM = "2.SVM";
my $path_MB = "3.Model-Building";
my $path_PF = "4.Probe-Finding";
my $path_MT = "5.Model-Testing";
my $path_parameters;

#perl scripts run in pipeline
my $file_MFA = "Submit-MFAPreselection.For-Cedar.sh",
my $file_SVM = "SVM-Gene-List-Filter-and-Printer-GEOnly.GENENAME-and-TAB-FIXED_edit.pl";
my $file_MB_misclass = "MatLab-Script-Writer-for-Job-Split.Misclass_edit.pl";
my $file_MB_logloss = "MatLab-Script-Writer-for-Job-Split_edit.pl";
my $file_MB_summarize = "Jonathan-Program-Results-Summarizer.pl";
my $file_PF = "Model-Probe-FinderGPL96_edit.pl";
my $file_PF2 = "Probe-ID-Averaging-Program_edit.pl";
my $file_MT = "Perl-Shell.regularValidation_multiclassSVM.Multiple-Model-Version_edit.pl";
my $file_MT2 = "Regular-Validation-Results-Program-Output-Organizer.pl";

my $start = 0;
my $status;
my $continue = 0;

open CONFIG, "<pipeline_config.txt" or die $!;
runPipeline();
close CONFIG;

sub runPipeline {
$path_parameters = $step0."deg0-".$step1."deg1-".$step2."deg2";
my $try_mkdir = `mkdir $path_parameters`;
if (index($try_mkdir, "File exists") eq -1) { #copy folders if parameter directory doesn't already exist
    system("cp -r $path_SVM $path_parameters");
    system("cp -r $path_MB $path_parameters");
    system("cp -r $path_PF $path_parameters");
    system("cp -r $path_MT $path_parameters");
}

#set paths to correct new path
$path_SVM = $path_parameters."/".$path_SVM;
$path_MB = $path_parameters."/".$path_MB;
$path_PF = $path_parameters."/".$path_PF;
$path_MT = $path_parameters."/".$path_MT;

if ($boolean_svm eq "yes") { 

    #copy required files and run SVM program
    
    system("cp ./MFA.tsv $path_SVM");
    system("cp -r ./SVM $path_SVM");
    chdir $path_SVM;
    system("perl $file_SVM $step0 $step1 $step2 $labelFile");

    #copy outputted files to main folder
    system("cp Label.txt Genes.txt Features.Numerical-Matrix.txt Features.With-Gene-Header.txt ../.");
    chdir "../../";
}

=pod
(3) Model Building
Copy required files from 2.SVM

=cut
if ($boolean_modelBuilding eq "yes") {
    my $misclassification = "no";
    my $logLoss = "no";
    
    $start = 0;
    while (<CONFIG>){
        if (($start eq 1) && ($_ eq "\n")) {
            $start = 0;
            last;
        }

        if ($start eq 1){
            chomp;
            my @columns = split ("\t", $_);
            my $descript = $columns[0];
            my $result = $columns[1];
            if ($descript eq "Misclassification?") {
                $misclassification = $result;
                print "misclassification: $result \n"
            } elsif ($descript eq "Logloss?") {
                $logLoss = $result;
            }
        }

        if ($_ eq "(3)\tModel Building\n") { $start = 1; print "entering model building loop\n";}
    }

    #copy required files from main folder to model building folder
    #Genes.txt not used by model building program, omitted
    chdir $path_parameters;
    system("cp Label.txt Features.Numerical-Matrix.txt Features.With-Gene-Header.txt Genes.txt ../$path_MB"); #copy from $path_parameter
    chdir "../$path_MB";
    print `pwd`."\n";

    #to do
    if ($misclassification eq "yes"){
        print("Misclassification = yes\n");
        print `pwd`; #remove later; debugging
        system("perl $file_MB_misclass");
    }

    if ($logLoss eq "yes"){
        print("LogLoss = yes");
        system("perl $file_MB_logloss");
    }

    #check if jobs are complete
    checkJobComplete("Output.Misclassification.batchJobs.out");
    system("perl $file_MB_summarize");
    system("mv Annotated-And-Organized-Results.txt ../Model.txt");
    chdir "../../";
}

=pod
(4) Probe Finding
Copy required files from 3. Model Building
Need to have platform and series files -- should these be in the main folder or in 4.ProbeFinding
Model.txt is in parameter folder
=cut
if ($boolean_probeFinding eq "yes") {
    my $file_platform;
    my $file_series;

    $start = 0;
    while (<CONFIG>){
        if (($start eq 1) && ($_ eq "\n")) {
            $start = 0;
            last;
        }

        if ($start eq 1){
            chomp;
            my @columns = split ("\t", $_);
            my $descript = $columns[0];
            my $result = $columns[1];
            if ($descript eq "Platform File") {
                $file_platform = $result;
            } elsif ($descript eq "Series File") {
                $file_series = $result;
            }
        }

        if ($_ eq "(4)\tProbe Finding\n") { $start = 1;}
    }

    system("cp $path_parameters/Model.txt $path_PF");
    system("cp $file_platform $path_PF");
    system("cp $file_series $path_PF");
    chdir $path_PF;
    system("perl $file_PF $file_platform");
    system("perl $file_PF2 $file_series"); 
    system("cp Output.Averaged-Probes.Transposed.txt ../PatientGE.tsv");
    system("cp Output.Skipped-genes.txt ../");
    chdir "../../";

}

=pod
(5) Model Testing
Copy required files from 4.Probe-Finding

=cut
if ($boolean_modelTesting eq "yes") {
    my $svm_file;
    open OUTREQUIREDFILES, ">Required-Files.txt" or die $!;

    $start = 0;
    while (<CONFIG>){
        print "$_";
        if (($start eq 1) && ($_ eq "\n")) {
            $start = 0;
            last;
        }

        if ($start eq 1){
            print OUTREQUIREDFILES "$_";
            print $_;
            chomp;
            my @columns = split ("\t", $_);
            my $descript = $columns[0];
            my $result = $columns[1];
            if ($descript eq "TrainingData") {
                $svm_file = $result;
            }
        }

        if ($_ eq "(5)\tModel Testing\n") { $start = 1; print "entering model testing loop";}

    }

    close OUTREQUIREDFILES;

    #copy required files from ./4.Probe-Finding to ./5.Model-Testing
    system("cp $path_parameters/PatientGE.tsv $path_MT");
    system("cp $path_parameters/Model.txt $path_MT");
    system ("cp $path_parameters/Output.Skipped-genes.txt $path_MT"); #forgot the enter end directory

    #copy required files from main folder to ./5.Model-Testing
    system("cp Label.tsv $path_MT");
    system("cp SVM/$svm_file $path_MT");

    system("mv Required-Files.txt $path_MT"); #move Required-Files.txt from main folder to model testing folder
    chdir $path_MT;
    system("perl $file_MT");
    checkJobComplete("Output.ModelTesting.batchJobs.out");
    system("perl $file_MT2");
    system("cp  output.Regular-Validation-Program-Output-Organizer.Summarized.txt ../../Output_models/Model_$path_parameters.txt");
    chdir "../../";
}
}

#method to check all jobs in a process are complete before moving on to next step
sub checkJobComplete 
{
    my ($file) = @_;
    print "check job function";
    open JOBSMISMATCH, "<$file" or die $!;

    while (<JOBSMISMATCH>) {
        chomp;
        $continue = 0;
        $start = 0;
        LINE: while ($continue eq 0){
            $status = `sacct -j $_`;
            #my @status_lines = split("\n", $status);
            if ($start eq 0){
                print("$status\n");
            }

            if ((index($status, "PENDING") != -1) || (index($status, "RUNNING") != -1)) {
                print "Job $_ still running.\n";
                $start = 1;
                sleep(15);
                next LINE;
            } elsif (index($status, "FAILED") != -1){
                print "Job $_ failed.\n";
                print("$status\n");
                die;
            } elsif (index($status, "COMPLETED")) {
                $continue = 1;
                print "Job $_ completed.\n";
                print("$status\n");
            } else {
                print "Something went wrong";
                print("$status\n"); #all other scenarios
                die;
            }
            sleep(15);
        }
    }
    close JOBSMISMATCH;
}