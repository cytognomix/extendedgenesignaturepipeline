#!usr/bin/perl

use warnings;
use strict;

# this program will take the probe with ID file
# Model-With-Probes.txt
#
# and read in all of the data from the microarray file
# GSE61676_series_matrix.txt
#
# and addd up and average each probe for each gene, for all individuals on the list and print it into a file
# the input will need to be inverted: patients on left, genes on top. This should be done at the end.

#input arguments on command line
#should be of format perl Probe-ID-Averaging_Platform.pl seriesFile.txt
my $num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "\nUsage : Probe-ID-Averaging_Platform.pl seriesFile\n";
    die('Series file must be specified' . "\n");
}
my ($seriesFile) = @ARGV;


open DATA, "<$seriesFile" or die $!;

open OUTPUT, ">Output.Averaged-Probes.txt" or die $!;
open OUTPUT_SKIPPEDGENES, ">Output.Skipped-genes.txt" or die $!;

my %probeinfo;


while (<DATA>) {
	chomp;

	next if ($_ =~ /^!/);

	# print list of patient IDs
	if ($_ =~ /^\"ID_REF/) {
		$_ =~ s/\"//g;
		print OUTPUT "$_\n";
		next;
	}

	my @columns = split ("\t", $_);

	my $probeID = shift (@columns);

	my $newloopflag = 0;
	foreach(@columns) {
		
		if ($newloopflag == 0) {
			$probeinfo{$probeID} = $_;
		} else {
			$probeinfo{$probeID} .= "\t$_";
		}

		$newloopflag = 1;
	}


}

close DATA;


open PROBES, "<Model-With-Probes.txt" or die $!;
my %hashSkippedGenes;

while (<PROBES>) {
	chomp;
	my @columns = split ("\t", $_);
    my $length = @columns;

    #skip gene if no probe found
    if ($length == 1){
        $hashSkippedGenes{shift(@columns)} = 1;
        next;
    }

	my $gene = shift(@columns);
	# the rest should be probeIDs
	
	my @patientdata;
	my @skipcounter;
	my $probecounter = 0;

	foreach(@columns) {
		my $probeID = $_;
		my $loopcounter = 0;

		if (exists $probeinfo{$probeID}) {
			my @expression = split ("\t", $probeinfo{$probeID});

			foreach(@expression) {
				my $dataloop = $_;
				

				if ($probecounter == 0) {
					if ($dataloop =~ /[0-9]/) {
						# if it's a number
						$patientdata[$loopcounter] = $dataloop;
						$skipcounter[$loopcounter] = 0;
					} else {
						# this means the value isn't a number, therefore it's something like "N/A"
						$patientdata[$loopcounter] = 0;
                                                $skipcounter[$loopcounter] = 1;
					}
						

				} else {
					if ($dataloop =~ /[0-9]/) {
						$patientdata[$loopcounter] += $dataloop;

					} else {
						$skipcounter[$loopcounter] += 1;
					}

				}

				$loopcounter++;
			}

			$probecounter++;
		} else {
			# this would only happen if there are probes in the library file not in the data file	
			next;

			# it didd happen, so the following is commented out
			
			#print "This should not happen.\n";
			#die;
		}



	}

	print OUTPUT "$gene";

	my $foreachcounter = 0;

	foreach(@patientdata) {

		my $probetodivide = $probecounter - $skipcounter[$foreachcounter];
		
		if ($probetodivide == 0) {
			print "There is a patient that has nothing but N/A for this result. They will be assigned a \"0\".\n";
			$probetodivide = 1;
			
		}

		my $results = $_/$probetodivide;

		print OUTPUT "\t$results";
	
		$foreachcounter++;
	}
	print OUTPUT "\n";

}

my $ctr = 0;
foreach my $key (keys %hashSkippedGenes){
    
    if ($ctr == 0){
        print OUTPUT_SKIPPEDGENES "$key";
    } else {
        print OUTPUT_SKIPPEDGENES "\t$key";
    }
    $ctr ++;
}


close OUTPUT_SKIPPEDGENES;
close OUTPUT;


open TOINVERT, "<Output.Averaged-Probes.txt" or die  $!;

open OUT, ">Output.Averaged-Probes.Transposed.txt" or die $!;

my @rows = ();
my @transposed = ();

my $transcount = 0;

while (<TOINVERT>) {

	chomp;
	my @columns = split ("\t", $_);
	my $innerloopcount = 0;

	foreach(@columns) {
		if ($transcount == 0) {
			$transposed[$innerloopcount] = $_;
			

		} else {
			$transposed[$innerloopcount] .= "\t$_";


		}
		$innerloopcount++;
	} 
	$transcount++;
}

close TOINVERT;

foreach(@transposed) {

	print OUT "$_\n";

}


close OUT;
