#!usr/bin/perl

use warnings;
use strict;

# this program reads in a list of genes (Model.txt)
# and reads in the microarray library file for GPL5188-122.txt
# and outputs the probeIDs for those particular genes
# since this is an exon microarray, I have coded it to only include CORE probes

my %genesofmodel;
my @genelist;
my $count = 0;


open FILE, "<Model.txt" or die $!;

while (<FILE>) {
	chomp;
	my @columns = split ("\t", $_);

	foreach(@columns) {
			
		unless (exists $genesofmodel{$_}) {
			$genelist[$count] = $_;
			$count++;
			$genesofmodel{$_} = 1;
		}
	}



}

close FILE;


open LIBRARY, "<GPL96-57554.txt" or die $!;

open OUTPUT, ">Model-With-Probes.txt" or die $!;

my %probelist;


while (<LIBRARY>) {

	chomp;
	my @columns = split ("\t", $_);

	next if ($_ =~ /^#/);
	next if ($_ =~ /^ID/);

	# example input 
	# 1007_s_at	U48705		Homo sapiens	6-Oct-14	Exemplar sequence	Affymetrix Proprietary Database	U48705 /FEATURE=mRNA /DEFINITION=HSU48705 Human receptor tyrosine kinase DDR gene, complete cds	U48705	discoidin domain receptor tyrosine kinase 1 /// microRNA 4640	DDR1 /// MIR4640	780 /// 100616237	NM_001202521 /// NM_001202522 /// NM_001202523 /// NM_001954 /// NM_013993 /// NM_013994 /// NR_039783 /// XM_005249385 /// XM_005249386 /// XM_005249387 /// XM_005249389 /// XM_005272873 /// XM_005272874 /// XM_005272875 /// XM_005272877 /// XM_005275027 /// XM_005275028 /// XM_005275030 /// XM_005275031 /// XM_005275162 /// XM_005275163 /// XM_005275164 /// XM_005275166 /// XM_005275457 /// XM_005275458 /// XM_005275459 /// XM_005275461 /// XM_006715185 /// XM_006715186 /// XM_006715187 /// XM_006715188 /// XM_006715189 /// XM_006715190 /// XM_006725501 /// XM_006725502 /// XM_006725503 /// XM_006725504 /// XM_006725505 /// XM_006725506 /// XM_006725714 /// XM_006725715 /// XM_006725716 /// XM_006725717 /// XM_006725718 /// XM_006725719 /// XM_006725720 /// XM_006725721 /// XM_006725722 /// XM_006725827 /// XM_006725828 /// XM_006725829 /// XM_006725830 /// XM_006725831 /// XM_006725832 /// XM_006726017 /// XM_006726018 /// XM_006726019 /// XM_006726020 /// XM_006726021 /// XM_006726022 /// XR_427836 /// XR_430858 /// XR_430938 /// XR_430974 /// XR_431015	0001558 // regulation of cell growth // inferred from electronic annotation /// 0001952 // regulation of cell-matrix adhesion // inferred from electronic annotation /// 0006468 // protein phosphorylation // inferred from electronic annotation /// 0007155 // cell adhesion // traceable author statement /// 0007169 // transmembrane receptor protein tyrosine kinase signaling pathway // inferred from electronic annotation /// 0007565 // female pregnancy // inferred from electronic annotation /// 0007566 // embryo implantation // inferred from electronic annotation /// 0007595 // lactation // inferred from electronic annotation /// 0008285 // negative regulation of cell proliferation // inferred from electronic annotation /// 0010715 // regulation of extracellular matrix disassembly // inferred from mutant phenotype /// 0014909 // smooth muscle cell migration // inferred from mutant phenotype /// 0016310 // phosphorylation // inferred from electronic annotation /// 0018108 // peptidyl-tyrosine phosphorylation // inferred from electronic annotation /// 0030198 // extracellular matrix organization // traceable author statement /// 0038063 // collagen-activated tyrosine kinase receptor signaling pathway // inferred from direct assay /// 0038063 // collagen-activated tyrosine kinase receptor signaling pathway // inferred from mutant phenotype /// 0038083 // peptidyl-tyrosine autophosphorylation // inferred from direct assay /// 0043583 // ear development // inferred from electronic annotation /// 0044319 // wound healing, spreading of cells // inferred from mutant phenotype /// 0046777 // protein autophosphorylation // inferred from direct assay /// 0060444 // branching involved in mammary gland duct morphogenesis // inferred from electronic annotation /// 0060749 // mammary gland alveolus development // inferred from electronic annotation /// 0061302 // smooth muscle cell-matrix adhesion // inferred from mutant phenotype	0005576 // extracellular region // inferred from electronic annotation /// 0005615 // extracellular space // inferred from direct assay /// 0005886 // plasma membrane // traceable author statement /// 0005887 // integral component of plasma membrane // traceable author statement /// 0016020 // membrane // inferred from electronic annotation /// 0016021 // integral component of membrane // inferred from electronic annotation /// 0043235 // receptor complex // inferred from direct assay /// 0070062 // extracellular vesicular exosome // inferred from direct assay	0000166 // nucleotide binding // inferred from electronic annotation /// 0004672 // protein kinase activity // inferred from electronic annotation /// 0004713 // protein tyrosine kinase activity // inferred from electronic annotation /// 0004714 // transmembrane receptor protein tyrosine kinase activity // traceable author statement /// 0005515 // protein binding // inferred from physical interaction /// 0005518 // collagen binding // inferred from direct assay /// 0005518 // collagen binding // inferred from mutant phenotype /// 0005524 // ATP binding // inferred from electronic annotation /// 0016301 // kinase activity // inferred from electronic annotation /// 0016740 // transferase activity // inferred from electronic annotation /// 0016772 // transferase activity, transferring phosphorus-containing groups // inferred from electronic annotation /// 0038062 // protein tyrosine kinase collagen receptor activity // inferred from direct assay /// 0046872 // metal ion binding // inferred from electronic annotation

	
	my $probeID = $columns[0];
	my $gene = $columns[10];
	next if ($gene eq "---");
	
	my $main = pop (@columns);
	#my $probetype = pop(@columns);
	
	#next unless ($probetype eq "core");


	# genes with multiple names are divided by ///
	my @genelistsplit = split (" /// ", $gene);

	next if ($gene eq "");
	#print "$genelistsplit[0]\n";

	my $loopcounter = 0;
	my $previousgene = "";

	foreach(@genelistsplit) {
		#my $geneloop = $_;


		#my @genesplit = split (" // ", $geneloop);

		my $geneofinterest = $genelistsplit[0];

		if ($loopcounter == 0) {
			$previousgene = $geneofinterest;

		} else {
			if ($previousgene ne $geneofinterest) {
				print "Two gene probe! It does exist!\n";
				die;
			}

		}

		$loopcounter += 0;
	}

	if (exists $genesofmodel{$previousgene}) {
		if (exists $probelist{$previousgene}) {
			$probelist{$previousgene} .= "\t$probeID";

		} else {
			$probelist{$previousgene} = "$probeID";		
		}
		#	print OUTPUT "$previousgene\t$probeID\n";
	}

#	sleep(1);

}

close LIBRARY;

foreach (@genelist) {
	print OUTPUT "$_\t$probelist{$_}\n";

}


close OUTPUT;


