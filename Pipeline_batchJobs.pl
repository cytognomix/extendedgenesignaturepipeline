=pod
Date: 28-10-2019
Author: Michelle Ho
Pipeline for model generation and testing
Required files in same folder as Pipeline,py:
    1. pipeline_config.txt
        Format:
            MFA? yes/no
            SVM gene filtering? yes/no
            Model Building? yes/no
            Probe Finding? yes/no
            Model Testing? yes/no
            (...)
Required folders downstream of Pipeline.py:
    1. MFA-Preselection
    2. SVM gene filtering
            step0	30  10  #second number is end value
            step1	20  10
            step2	10  10
            Increment by    2 #go down to 0, if less than 0 then = 0; set at 0 if just 1 run
            LabelFile	18.tsv
    3. Model Building
    4. Probe Finding
    5. Model Testing

=cut

use warnings;
use strict;

open CONFIG, "<pipeline_config.txt" or die $!;

#boolean values set from config file, whether or not to run each step
my $boolean_mfa;
my $boolean_svm;
my $boolean_modelBuilding; #shortened to MB
my $boolean_probeFinding; #shortened to PF
my $boolean_modelTesting; #shortened to MT

#pathway to directories for steps 1 through 5
my $path_MFA = "1.PerformingMFA";
my $path_SVM = "2.SVM";
my $path_MB = "3.Model-Building";
my $path_PF = "4.Probe-Finding";
my $path_MT = "5.Model-Testing";
my $path_parameters;

#perl scripts run in pipeline
my $file_MFA = "Submit-MFAPreselection.For-Cedar.sh",
my $file_SVM = "SVM-Gene-List-Filter-and-Printer-GEOnly.GENENAME-and-TAB-FIXED_edit.pl";
my $file_MB_misclass = "MatLab-Script-Writer-for-Job-Split.Misclass_edit.pl";
my $file_MB_logloss = "MatLab-Script-Writer-for-Job-Split_edit.pl";
my $file_MB_summarize = "Jonathan-Program-Results-Summarizer.pl";
my $file_PF = "Model-Probe-FinderGPL96_edit.pl";
my $file_PF2 = "Probe-ID-Averaging-Program_edit.pl";
my $file_MT = "Perl-Shell.regularValidation_multiclassSVM.Multiple-Model-Version_edit.pl";
my $file_MT2 = "Regular-Validation-Results-Program-Output-Organizer.pl";

my $start = 0;
my $status;
my $continue = 0;

=pod
Extract information from first 5 lines of pipeline_config.txt
MFA?	yes/no
SVM gene filtering?	yes
Model Building?	yes/no
Probe Finding?	yes/no
Model Testing?	yes/no
=cut

my $found = 0;
while (<CONFIG>) {

	chomp;
	my @columns = split ("\t", $_);

	my $descript = $columns[0];
	my $result = $columns[1];

	if ($descript eq "MFA?") {
		$boolean_mfa = $result;
		$found++;
	} elsif ($descript eq "SVM gene filtering?") {
		$boolean_svm = $result;
		$found++;
	} elsif ($descript eq "Model Building?") {
		$boolean_modelBuilding = $result;
		$found++;
	} elsif ($descript eq "Probe Finding?") {
		$boolean_probeFinding = $result;
		$found++;
	} elsif ($descript eq "Model Testing?") {
		$boolean_modelTesting = $result;
		$found++;
	}

    if ($found eq 5) {
        last;
    }
}
=pod
(1) MFA Preselection
drug		Paclitaxel
genesInitial	ABCC10	BCL2	BCL2L1	BIRC5	BMF	FGF2	FN1	MAP4	MAPT	NFKB2	SLCO1B3	TLR6	TMEM243	TWIST1	CSAG2
aliasesFile	SVM-Modelling-Project/GeneNames-Association.Pseudonyms.txt
relationsFile	SVM-Modelling-Project/PathwayCommons.OneNode.InteractionFile.txt.sif
gi50sFile	SVM-Modelling-Project/GI50-Data.txt
copiesFile	SVM-Modelling-Project/CopyNumber-Data.txt
expressionsFile	SVM-Modelling-Project/GeneExpression-Data.txt
angleCutoff	70
stepsCutoff	2
circleOutput	False
mfaInput        False
mfaOutput       True
svmInput        True
aliasOutput	True 
=cut

if ($boolean_mfa eq "yes"){
    open OUTMFACONFIG, ">config.txt" or die $!;

    $start = 0;
    while (<CONFIG>){
        print $_;
        if (($start eq 1) && ($_ eq "\n")) {
            $start = 0;
            last;
        }

        if ($start eq 1){
            my @columns = split ("\t", $_);
            if ($columns[0] eq "genesInitial"){
                my $file = $columns[1];
                my $genelist = "";
                system("dos2unix $file"); #formatting otherwise get ^M character after each gene
                open GENELIST, "<$file";
                while (<GENELIST>){
                    chomp;
                    $genelist = $genelist."\t".$_;
                }
                close GENELIST;
                print OUTMFACONFIG $columns[0].$genelist;
            }
            else{
                print OUTMFACONFIG "$_";
            }
        }

        if ($_ eq "(1)\tMFA\n") { $start = 1;}

    }

    close OUTMFACONFIG;
    system("mv config.txt $path_MFA");
    chdir $path_MFA;
    print("Running MFA\n");
    $status = `sbatch --parsable $file_MFA`;
    print("Submitted batch job $status");

    # sleep(10);
    #checkJobComplete($status);
    #system("cp -r MFA.tsv SVM ../");
}


=pod
(2) SVM Gene Filtering
step0	20
step1	20
step2	20
LabelFile	18.Paclitaxel-15genes.tsv

Copy required files from 1.MFA-Preselection (move files to main)
    Genes.txt
    Label.txt
    Features.Numerical-Matrix.txt
    Features.With-Gene-Header.txt
    SVM Folder
Extract parameters and run > perl SVM-Gene-Filter.pl step0 step1 step2
Should the program die if invalid step0, step1, step2 degrees?
Creates folder for that parameter e.g. 30deg0-20deg1-10deg2
=cut

#Regardless of whether SVM is being run, need to create folder for that parameter set being run
#The next portion of code will only be run if SVM, model building, probe finding, or model testing steps are required (Not performed if only MFA is run)
if (($boolean_svm eq "yes") || ($boolean_modelBuilding eq "yes") || ($boolean_probeFinding eq "yes") || ($boolean_modelTesting eq "yes")) {
    my $step0;
    my $step1;
    my $step2;
    my $increment;
    my $labelFile;
    my ($step0_start, $step1_start, $step2_start);
    my ($step0_end, $step1_end, $step2_end);

    #extract information from pipeline_config.txt
    $start = 0;
    while (<CONFIG>){
        if (($start eq 1) && ($_ eq "\n")) {
            $start = 0;
            last;
        }

        if ($start eq 1){
            chomp;
            my @columns = split ("\t", $_);
            my $descript = $columns[0];
            my $result = $columns[1];
            if ($descript eq "step0") {
                $step0 = $step0_start = $result;
                $step0_end = $columns[2];
            } elsif ($descript eq "step1") {
                $step1 = $step1_start = $result;
                $step1_end = $columns[2];
            } elsif ($descript eq "step2") {
                $step2 = $step2_start = $result;
                $step2_end = $columns[2];
            } elsif ($descript eq "Increment by") {
                $increment = $result;
            }elsif ($descript eq "LabelFile") {
                $labelFile = $result;
            }
        }

        if ($_ eq "(2)\tSVM gene filtering\n") { $start = 1;}
    }

    ##start for loops here, iterates through different parameters##
    system("mkdir Output_models"); #Make folder to store output model files if folder doesn't exist yet
    my $shell_parameters;
    while ($step0 >= $step0_end){
        $step1 = $step1_start;
        while ($step1 >= $step1_end){
            $step2 = $step2_start;
            while ($step2 >= $step2_end){
                #runPipeline();
                $shell_parameters = $step0."deg0-".$step1."deg1-".$step2."deg2.sh";
                open SHELL, "> $shell_parameters" or die $!;
                print SHELL qq(#!/bin/bash\n);
                print SHELL qq(#SBATCH --mem-per-cpu=4096M\n);
                print SHELL qq(#SBATCH --time=5-00:00\n);
                print SHELL qq(#SBATCH --account=def-progan\n);
                print SHELL qq(#SBATCH --output=$shell_parameters.log\n);
                print SHELL qq(perl RunPipeline.pl $boolean_svm $boolean_modelBuilding $boolean_probeFinding $boolean_modelTesting $labelFile $step0 $step1 $step2\n); 
                print SHELL qq(rm $shell_parameters\n);

                close SHELL;

                print "Running batch file $shell_parameters";

                #check if job was actually submitted
                $status =  `sbatch $shell_parameters`;
                print $status;
                sleep(5);
                while ((index($status, "Submitted") == -1)) {
                    $status =  `sbatch $shell_parameters`;
                    print "\t + $status";
                    sleep(5);
                }
                
                $step2 = $step2 - $increment;
            }
            $step1 = $step1 - $increment;
        }
    $step0 = $step0 - $increment;
    }   
}

sub checkJobComplete 
{
    my ($job) = @_;
    print "check job function";
    $status = `squeue -j $job`;
    print("$status\n");

    LINE: while ($continue eq 0){
        if ((index($status, "PD") != -1) || (index($status, "RUNNING") != -1)) {
            print "Job $job still running.\n";
            sleep(15);
            next LINE;
        } elsif (index($status, "FAILED") != -1){
            print "Job $job failed.\n";
            print("$status\n");
            die;
        } elsif ((index($status, "COMPLETED") != -1) || (index($status, $job) == -1)) {
            $continue = 1;
            print "Job $job completed.\n";
            print("$status\n");
        } else {
            print "Something went wrong";
            print("$status\n"); #all other scenarios
            die;
        }
    }
}
