=pod
Author: Michelle Ho
For Topotecan models (probe finding step skipped)
Checks if PatientGE files miss any genes included in the models and write that out to Output.Skipped-genes.txt
=cut

use warnings;
use strict;

my $file_input = "PatientGE_Microarray.tsv";
my $file_output = "Output.Skipped-genes.txt";
my $file_model = "Model.txt";

open INPUT, "<$file_input" or die $!;
open MODEL, "<$file_model" or die $!;
#open OUTPUT, ">$file_output" or die $!;

my $line1 = <INPUT>;
#print($line1."\n");
my %genes = map {$_ => 1} split("\t", $line1);
my @keys = keys %genes;
my %nonexistent;

<MODEL>; #skip first line
while(<MODEL>){
    if ($_ eq "\n"){
        last;
    }
    chomp;
    my @columns = split("\t", $_);
    splice(@columns, 0, 3);
    foreach my $gene (@columns){
        if (!exists($genes{$gene})){
            $nonexistent{$gene} = 1;
        } 
    }
    print join("\t", @columns);
    print("\n");

}

my @notthere = keys %nonexistent;
print("Missing in Patient GE: ");
print join("\t", @notthere);

close INPUT;
close MODEL;

open OUTPUT, ">$file_output" or die $!;
my $ctr = 0;
foreach (@notthere){
    if($ctr == 0) {
        print OUTPUT $_;
    } else {
        print OUTPUT "\n".$_;
    }
    $ctr ++;    
}
close OUTPUT;

print;
#system("cp test.sdf .");
