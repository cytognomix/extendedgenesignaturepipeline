#!/bin/bash
#SBATCH --mem 6000M
#SBATCH -t 7-00:00:00
#SBATCH -o MFA-Log-File.txt
#SBATCH --account=def-progan
module load nix
nix-env -i /home/tyson/.nix-profile/bin/MFAPreselection
MFAPreselection
