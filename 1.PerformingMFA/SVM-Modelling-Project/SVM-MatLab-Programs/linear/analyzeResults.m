function [Precision, Recall, Accuracy] = analyzeResults(y_predict, y_label)
%function [FN, Precision, Accuracy, Recall] = analizeResults(X)
sizeX = size(y_predict);
numX = sizeX(1);

TP = 0;
FP = 0;
TN = 0;
FN = 0;

for i=1:numX
    
   % if prediction is 1 
   if y_predict(i) == 1
       
       % if prediciton matches patient outcome
       if y_predict(i) == y_label(i)
           TP = TP+1;   
       else
           FP = FP+1;
       end   
   end
    
   %if prediction is 0
   if y_predict(i) == 0
       
       % if prediciton matches patient outcome
       if y_predict(i) ==y_label(i)
           TN = TN+1;   
       else
           FN = FN+1;
       end   
   end
    
end

% calculate precision
if TP+FP==0
    Precision = -1;
else
    Precision = TP/(TP+FP);
end

%calculate recall
if TP+FN==0
    Recall = -1;
else
    Recall = TP/(TP+FN);
end

%calculate accuracy
if TP+FP+FN+TN == 0
    Accuracy = -1;
else
    Accuracy = (TP+TN)/(TP+FP+FN+TN);
end
