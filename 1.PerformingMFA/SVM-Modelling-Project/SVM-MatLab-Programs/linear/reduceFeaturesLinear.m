function [Features_final, minC] = reduceFeaturesLinear(Features, y, C)
% Function that takes cell array of features with labels and reduces features to minimize
% classification error. Returns the final features and the minimum
% classification error achieved.

% Features contains feature values with feature label- cell array
% y contains just binary values, no label

Feature_temp = Features;        %remove labels from feature cell array and turn into matrix
Feature_temp(1,:) = [];
Train = cell2mat(Feature_temp);

minC = 1;   % initialize minimum misclassification score to max value
n = size(Train); % number of features in input matrix

for k=1:n(2)-1   % loop until missclassification stops decreasing 

Cost = calculateScoresLinear(Train, y, C);  % calculate missclasification scores for current Feature set

[M, I] = min(Cost);                % find index and value of feature which leads to best missclasification value
Train(:,I) = [];
Features(:,I) = [];

if M <= minC                    % if lowest value is less then the lowest value achieved delete feature and update min error and final features
    Features_final = Features;
    minC = M;
end
end
end