function [min_error,C_best, Features_best] = optimizeLinearParametersParallel(Features, y)
% runs reduceFeatures for different values of sigma and C between 1e-5 and
% 1e5 to find optimal C and sigma values. Returns C and sigma values, minimum
% classification error and corresponding features. 

%C = 1e5;   % initialize C
C = 1e-5;

Features_Cell = cell(121,1);    %will store best features for each iteration
Error_vals = ones(121,1);       %will stores error values for each iteration
C_vals = ones(121,1);            %will stores C values for each iteration

parfor iter = 1:11
    
    % find C value for current iteration
    %C_current = C / 10^(iter-1);
    C_current = C * 10^(iter-1);
    
    % run reduce features for current C and sigma values
    [Features_final, minC] = reduceFeaturesLinear(Features, y, C_current);
    
    % store results
    Features_Cell{iter} = Features_final;
    Error_vals(iter) = minC;
    C_vals(iter) = C_current;
    
    % notify user of progress
    fprintf('Step %i of 121 complete\n',iter);
     
end

%get results
[min_error, index] = min(Error_vals);
C_best = C_vals(index);
Features_best = Features_Cell{index};

