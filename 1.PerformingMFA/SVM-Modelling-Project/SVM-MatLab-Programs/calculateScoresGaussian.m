function [ Cost ] = calculateScoresGaussian( Train, y, C, sigma)
% Function that takes in Test data and finds missclassification score for the
% test data with each one of the features left out

n = size(Train); % number of features in input matrix
Cost = ones(n(2),1); % Matrix to store missclassfication - size m(number of training samples)


for k=1:n(2)    % repeat leaving out feature for all features
    
    X = Train;   
    X(:,k) = [];    % remove feature at position k in test data
    
    svmModel = fitcsvm(X, y,'Standardize',true,'KernelFunction', 'rbf', 'BoxConstraint', C,'KernelScale', sigma); % fit SVM
    %svmModel = fitcsvm(Train, y, 'KernelFunction', 'rbf');
    %CVSVMModel = crossval(svmModel, 'KFold', 12); % cross validates SVM
    CVSVMModel = crossval(svmModel, 'Leaveout', 'on');
    Cost(k,1) = kfoldLoss(CVSVMModel);   % store missclassification score for current left out feature

end

end

