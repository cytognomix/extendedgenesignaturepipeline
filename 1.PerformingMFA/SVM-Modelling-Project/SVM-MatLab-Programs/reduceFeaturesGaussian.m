function [Features_final, minC] = reduceFeaturesGaussian(Features, y, C, sigma)
% Function that takes cell array of features with labels and reduces features to minimize
% classification error. Returns the final features and the minimum
% classification error achieved. 

% Features contains feature values with feature label- cell array
% y contains just binary values, no label

%C and sigma are the tuning parameters for a Gaussian Kernel

Feature_temp = Features;        %remove labels from feature cell array and turn into matrix
Feature_temp(1,:) = [];
Train = cell2mat(Feature_temp);

minC = 1;   % initialize minimum misclassification score to max value
n = size(Train); % number of features in input matrix

for k=1:n(2)-1   % loop until missclassification stops decreasing 

Cost = calculateScoresGaussian(Train, y, C, sigma);  % calculate missclasification scores for current Feature set(Gaussian Kernel)

[M, I] = min(Cost);                % find index and value of feature removal which leads to best missclasification value and remove it from features
Train(:,I) = [];
Features(:,I) = [];

if M <= minC                    % if lowest value is less then the lowest value achieved update minmum C and best features
    Features_final = Features;
    minC = M;
end
end
end