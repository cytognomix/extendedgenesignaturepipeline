function [Features_Cell, Error_vals, C_vals, sigma_vals] = optimizeGaussianParametersParallel(Features, y)
% runs reduceFeatures for different values of sigma and C between 1e-5 and
% 1e5 to find optimal C and sigma values. Returns C and sigma values, minimum
% classification error and corresponding features. 

sigma = 1e-5; % initialize sigma
C = 1e5;   % initialize C

Features_Cell = cell(121,1);    %will store best features for each iteration
Error_vals = ones(121,1);       %will stores error values for each iteration
C_vals = ones(121,1);            %will stores C values for each iteration
sigma_vals = ones(121,1);        %will stores sigam values for each iteration

parfor iter = 1:121
    
    % find sigma value for current iteration
    sigma_increment = fix((iter-1)/11);
    sigma_current = sigma * 10^sigma_increment;
    
    % find C value for current iteration
    C_increment = mod((iter-1),11);
    C_current = C / 10^C_increment;
    
    % run reduce features for current C and sigma values
    [Features_final, minC] = reduceFeaturesGaussian(Features, y, C_current, sigma_current);
    
    % store results
    Features_Cell{iter} = Features_final;
    Error_vals(iter) = minC;
    C_vals(iter) = C_current;
    sigma_vals(iter) = sigma_current;
    
    % notify user of progress
    fprintf('Step %i of 121 complete\n',iter);
     
end

