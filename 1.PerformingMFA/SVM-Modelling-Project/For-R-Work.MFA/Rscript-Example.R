
Dataset <- 
  read.table("C:/Users/ROGANLAB/Desktop/Tyson-Pathway-Project/MFA-Result-Examples/PRKCA-MFA-Input-File.txt",
   header=TRUE, sep="\t", na.strings="NA", dec=".", strip.white=TRUE)
Dataset.MFA<-Dataset[, c("GI50", "CopyNumber", "GeneExpression")]
res<-MFA(Dataset.MFA, group=c(1, 1, 1), type=c("s", "s", "s"), ncp=5, name.group=c("GI50", "CopyNumber", "GeneExpression"), num.group.sup=c(), graph=FALSE)
res.hcpc<-HCPC(res ,nb.clust=-1,consol=FALSE,min=3,max=10,graph=TRUE)
plot.MFA(res, axes=c(1, 2), choix="group", new.plot=TRUE, lab.grpe=TRUE)
plot.MFA(res, axes=c(1, 2), choix="axes", new.plot=TRUE, habillage="group")
plot.MFA(res, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, habillage="group", lim.cos2.var=0)
plot.MFA(res, axes=c(1, 2), choix="ind", new.plot=TRUE, lab.ind=TRUE, lab.par=TRUE, lab.var=TRUE, habillage="group")
summary(res, nb.dec = 3, nbelements=10, nbind = 10, ncp = 3, file="")
write.infile(res$eig, file ="PRKCA",append=FALSE)
write.infile(res$group, file ="PRKCA",append=TRUE)
write.infile(res$inertia.ratio, file ="PRKCA",append=TRUE)
write.infile(res$ind, file ="PRKCA",append=TRUE)
write.infile(res$summary.quanti, file ="PRKCA",append=TRUE)
write.infile(res$quanti.var, file ="PRKCA",append=TRUE)
write.infile(res$partial.axes, file ="PRKCA",append=TRUE)
write.infile(dimdesc(res, axes=1:5), file ="PRKCA",append=TRUE)
remove(Dataset.MFA)

