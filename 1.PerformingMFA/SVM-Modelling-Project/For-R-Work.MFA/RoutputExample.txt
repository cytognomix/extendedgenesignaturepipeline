
> Dataset <- 
+   read.table("C:/Users/ROGANLAB/Desktop/Tyson-Pathway-Project/MFA-Result-Examples/PRKCA-MFA-Input-File.txt",
+    header=TRUE, sep="\t", na.strings="NA", dec=".", strip.white=TRUE)

> Dataset.MFA<-Dataset[, c("GI50", "CopyNumber", "GeneExpression")]

> res<-MFA(Dataset.MFA, group=c(1, 1, 1), type=c("s", "s", "s"), ncp=5, name.group=c("GI50", "CopyNumber", "GeneExpression"), num.group.sup=c(), graph=FALSE)

> res.hcpc<-HCPC(res ,nb.clust=-1,consol=FALSE,min=3,max=10,graph=TRUE)

> plot.MFA(res, axes=c(1, 2), choix="group", new.plot=TRUE, lab.grpe=TRUE)

> plot.MFA(res, axes=c(1, 2), choix="axes", new.plot=TRUE, habillage="group")

> plot.MFA(res, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, habillage="group", lim.cos2.var=0)

> plot.MFA(res, axes=c(1, 2), choix="ind", new.plot=TRUE, lab.ind=TRUE, lab.par=TRUE, lab.var=TRUE, habillage="group")

> summary(res, nb.dec = 3, nbelements=10, nbind = 10, ncp = 3, file="")

Call:
"res<-MFA(Dataset.MFA, group=c(1, 1, 1), type=c(\"s\", \"s\", \"s\"), ncp=5, name.group=c(\"GI50\", \"CopyNumber\", \"GeneExpression\"), num.group.sup=c(), graph=FALSE)" 


Eigenvalues
                       Dim.1   Dim.2   Dim.3
Variance               1.376   0.916   0.709
% of var.             45.861  30.519  23.620
Cumulative % of var.  45.861  76.380 100.000

Groups
                  Dim.1    ctr   cos2    Dim.2    ctr   cos2    Dim.3    ctr   cos2  
GI50           |  0.592 43.036  0.351 |  0.019  2.054  0.000 |  0.389 54.910  0.151 |
CopyNumber     |  0.288 20.917  0.083 |  0.672 73.359  0.451 |  0.041  5.724  0.002 |
GeneExpression |  0.496 36.046  0.246 |  0.225 24.587  0.051 |  0.279 39.366  0.078 |

Individuals (the 10 first)
                  Dim.1    ctr   cos2    Dim.2    ctr   cos2    Dim.3    ctr   cos2  
1              |  0.765  1.038  0.826 |  0.141  0.053  0.028 |  0.322  0.357  0.146 |
2              | -2.055  7.483  0.548 | -0.805  1.726  0.084 | -1.681  9.730  0.367 |
3              | -2.249  8.969  0.950 | -0.066  0.012  0.001 |  0.511  0.899  0.049 |
4              |  1.043  1.929  0.802 |  0.253  0.170  0.047 |  0.453  0.707  0.151 |
5              | -1.178  2.459  0.649 |  0.763  1.552  0.273 | -0.409  0.576  0.078 |
6              | -1.792  5.690  0.729 | -1.053  2.952  0.252 | -0.291  0.292  0.019 |
7              |  1.764  5.517  0.726 |  0.923  2.268  0.199 | -0.566  1.104  0.075 |
8              | -1.257  2.800  0.698 | -0.822  1.799  0.298 | -0.095  0.031  0.004 |
9              |  0.985  1.720  0.424 | -0.912  2.215  0.363 | -0.698  1.675  0.213 |
10             |  0.226  0.091  0.009 | -1.929  9.914  0.620 |  1.494  7.680  0.372 |

Continuous variables
                  Dim.1    ctr   cos2    Dim.2    ctr   cos2    Dim.3    ctr   cos2  
GI50           |  0.769 43.036  0.592 |  0.137  2.054  0.019 |  0.624 54.910  0.389 |
CopyNumber     | -0.536 20.917  0.288 |  0.820 73.359  0.672 |  0.201  5.724  0.041 |
GeneExpression |  0.704 36.046  0.496 |  0.474 24.587  0.225 | -0.528 39.366  0.279 |

> write.infile(res$eig, file ="PRKCA",append=FALSE)

> write.infile(res$group, file ="PRKCA",append=TRUE)

> write.infile(res$inertia.ratio, file ="PRKCA",append=TRUE)

> write.infile(res$ind, file ="PRKCA",append=TRUE)

> write.infile(res$summary.quanti, file ="PRKCA",append=TRUE)

> write.infile(res$quanti.var, file ="PRKCA",append=TRUE)

> write.infile(res$partial.axes, file ="PRKCA",append=TRUE)

> write.infile(dimdesc(res, axes=1:5), file ="PRKCA",append=TRUE)

> remove(Dataset.MFA)

