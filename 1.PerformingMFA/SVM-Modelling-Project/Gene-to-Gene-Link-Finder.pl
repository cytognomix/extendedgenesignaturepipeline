#!usr/bin/perl

use warnings;
use strict;

my $geneofinterest = $ARGV[0];

my @genes = ("BCL2", "PRKCA","CDKN2C","SNAI1","BARD1","ERCC2","PNKP","POLQ","MAP3K1","MAPK13","FOS","NFKB1","ATP7B","SLC22A5","BCL2L1","CFLAR","PRKCB","ERCC6","FAAP24","FEN1","MSH2","POLD1","GSTO1","GSTP1","MT2A","MAPK3","NFKB2","PRKAA2","TLR4","TP63","TWIST1","SLC31A2");


open FILE, "<PathwayCommons.OneNode.InteractionFile.txt.sif" or die $!;

while (<FILE>) {
	chomp;
	my $line = $_;

	my @columns = split ("\t", $line);

	my $gene1 = $columns[0];
	my $gene2 = $columns[2];

	foreach(@genes) {

		if (($gene1 eq $_) && ($gene2 eq $geneofinterest)) {
			print "$line\n";
		} elsif (($gene2 eq $_) && ($gene1 eq $geneofinterest)) {
			print "$line\n";
		}


	}

	


}

close FILE;
