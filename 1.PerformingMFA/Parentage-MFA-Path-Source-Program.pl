#!usr/bin/perl

use warnings;
use strict;


# this program will need two inputs
# a tab delimited model file, that gives a list of genes (GENE1	GENE2	GENE3...)
# and the MFA.tsv file of interest
# it will then report how each genes "path" through the MFA

# Model 1
# GENE1	Step 2	GENE1 -> Gene2


# run program: perl Parentage-MFA-Path-Source-Program.pl Model.txt MFA.tsv 
my $modelfile = $ARGV[0];
my $MFAfile = $ARGV[1];


open OUTPUT, ">output.Parentage-MFA-Path-Source-Program.txt" or die $!;

unless (defined $modelfile) {
	print "How to run program:\n";
	print "perl Parentage-MFA-Path-Source-Program.pl Modelfile MFAfile\n";
	die;

}

print "User warning: Program can handle genes up to 3 steps away. Data with 3-step genes have not been fully tested.\n";


open GENES, "< $modelfile" or die $!;
my $count = 1;

while (<GENES>) {
	chomp;
	my @columns = split ("\t", $_);

	if ($count > 1) {
		print OUTPUT "\n";
	}

	print OUTPUT "Model $count:\n";


	foreach(@columns) {

		my $geneofinterest = $_;

		open MFA, "< $MFAfile" or die $!;

		my %MFAinfo;
		my $zerostepflag = 0;

		while (<MFA>) {
			chomp;
			my @pieces = split ("\t", $_);
			next if ($pieces[0] eq "Gene");

			# A2M     176.0498718895423       91.94875961138597       copy    negative        1       CYP2C8:interacts-with
			my $gene = $pieces[0];
			my $step = $pieces[5];
			my $connection = $pieces[6];
			unless (defined $connection) {
				$connection = "";
			}

			$MFAinfo{$gene} = "$step\t$connection";

			if ($gene eq $geneofinterest) {
		        	if ($step == 0) {
					print OUTPUT "$geneofinterest\tStep 0\tNo Associated Genes\n";
					$zerostepflag = 1;
					last;
				}
			}

		}

		close MFA;
		next if ($zerostepflag == 1);

		my $interest = $MFAinfo{$geneofinterest};
		my @genedata = split ("\t", $interest);

		my $geneofintereststep = $genedata[0];
		my $associatedgene = $genedata[1];

		my @assocsplit = split /\|/, $associatedgene;

		print OUTPUT "$geneofinterest\tStep $geneofintereststep\t$geneofinterest to";

		my $firstloopflag = 0;

		foreach(@assocsplit) {
			my $data = $_;

			my @genename = split /\:/, $data;

			my $relatedgene = $genename[0];
			print OUTPUT " $relatedgene \($genename[1]\)" if ($firstloopflag == 0);
			print OUTPUT "  OR  $geneofinterest to $relatedgene \($genename[1]\)" if ($firstloopflag == 1);
	
	
			if ($geneofintereststep >= 2) {	
				my @nextsplit = split ("\t", $MFAinfo{$relatedgene});
				my $nextassociatedgene = $nextsplit[1];
				

				my @relatedsplitsplit = split /\|/, $nextassociatedgene;
				my $secondloopflag = 0;

				foreach(@relatedsplitsplit) {
					my @relatedgenename = split /\:/, $_;
					my $step2relatedgene = $relatedgenename[0];

					print OUTPUT " to $step2relatedgene \($relatedgenename[1]\)" if ($secondloopflag == 0);
					print OUTPUT "  OR  $geneofinterest to $relatedgene to $step2relatedgene \($relatedgenename[1]\)" if ($secondloopflag == 1);


					if ($geneofintereststep >= 3) {
						my @next3split = split ("\t", $MFAinfo{$step2relatedgene});
						my $next3associatedgene = $next3split[1];
						my @related3split = split /\|/, $next3associatedgene;

						my $thirdloopflag = 0;

						foreach(@related3split) {
							my @related3genename = split /\:/, $_;
							my $step3relatedgene = $related3genename[0];

							print OUTPUT " to $step3relatedgene \($related3genename[1]\)" if ($thirdloopflag == 0);
							print OUTPUT "  OR  $geneofinterest to $relatedgene to $step2relatedgene to $step3relatedgene \($related3genename[1]\)" if ($thirdloopflag == 1);
						
							 $thirdloopflag = 1;
						}


						if ($geneofintereststep >= 4) {
							print "WARNING! This program does not handle step 4 or higher genes.\n";
							die;

						}
					}
					$secondloopflag = 1;

				}


			} 



			$firstloopflag = 1;
		}
		print OUTPUT "\n";



	}



	$count++;


}

close GENES;

close OUTPUT;

print "\nCompleted. Please see file output.Parentage-MFA-Path-Source-Program.txt for output.\n";
