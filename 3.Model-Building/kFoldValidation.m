function [ misclassification, log_loss, goodness, prediction ] = kFoldValidation( Features, Labels, C, sigma, ends)
%KFOLDVALIDATION is an implementation of kfoldvalidation that allows the
%user to perform a STRATIFIED cross validation (can be used for
%non-stratified validation if needed) for SVM models
%   folds is a M x 2 cell where M is the number of folds; folds{i,1} stores
%   the features of fold i and folds{i,2} stores the labels of fold i
%   returns the number of incorrectly labelled samples and multi-class log
%   loss

m = size(Features,1); n = size(Features,2);
misclassification = 0;
goodness = 0;
prediction = zeros(length(Labels),1);

starts = [1];

for i=1:length(ends)-1
    starts = [starts ends(i)+1];
end

log_loss = 0;
for i=1:length(starts)
    start = starts(i); finish = ends(i);
    % get test features and test labels
    fea_test = Features(start:finish,:);
    label_test = Labels(start:finish,1);
    
    num_testsamples = finish - start + 1;
    
    % get training features and training labels
    fea_train = zeros(m-num_testsamples,n);
    label_train = zeros(m-num_testsamples,1);
    
    % samples before start
    fea_train(1:start-1,:) = Features(1:start-1,:);
    label_train(1:start-1,1) = Labels(1:start-1,1);
    
    % samples after finish
    fea_train(start:m-num_testsamples,:) = Features(finish+1:m,:);
    label_train(start:m-num_testsamples,1) = Labels(finish+1:m,1);
    
    % build SVM model
    t = templateSVM('KernelFunction', 'rbf', 'BoxConstraint', C, 'KernelScale', sigma);
    svmModel = fitcecoc(fea_train, label_train, 'learners', t, 'FitPosterior',1);
    [pred_i,~,~,Posterior] = predict(svmModel,fea_test); % posterior contains probabilities of a sample falling under each class
    
    % loop through each sample and accumulate the log_loss
    for j=1:length(pred_i)
        ind = find(svmModel.ClassNames == label_test(j)); % find index of ClassNames for the correct class of this sample
        % find probability of sample being in the correct class
        prob = max(min(Posterior(j,ind),1-10^-15),10^-15);
        log_loss = log_loss + log(prob);
    end
    
    % update num_correct and num_predictions
    misclassification = misclassification + sum(pred_i ~= label_test);
    goodness = goodness + sum(abs(pred_i - label_test));
    
    prediction(start:finish) = pred_i;
end

misclassification = misclassification / m;
log_loss = -log_loss / m;
goodness = goodness / m;

end

