function [ features_added, misclassification, time_elapsed ] = FFS_strat_kfold( Features, Label, ends, C, sigma, logloss)
%FFS_SVM_multiclass implements sequential forward feature selection using a
%multiclass SVM wrapper whose performance is evaluated by multi-class
%log loss on kfold validation
%   Features is a M x N matrix of m samples and n features
    % The features must be organized so that each continuous fold has a
    % good balance of all classes - do not group samples by classes
%   Label is a M x 1 vector of target variables (one for each sample)
%   ends is a vector that specifies that end of each fold
%   final_features is a vector of column numbers specifying the features
%   that were found to be the best subset
%   misclassification is the kfold validation error associated with the optimized
%   feature set
%   logloss is a boolean input; if true, log loss is used as the cost
%   function

tic;

%Features = zscore(Features);

temp_features = []; % start with empty feature set
% use LOOCV to evaluate performance of a linear regression model built from
% temp_features and Label
error_min = inf; % this error value is the temporary minimum that needs to be "beat"

num_features = size(Features,2);
features_remaining = 1:num_features;
features_added = [];

% while there are still features to add, add feature, calculate new errors and
% compare with previous best error
while size(temp_features,2) < num_features
    errors = [];
    % try adding each possible feature that could be added
    parfor i=1:length(features_remaining)
        temp_temp_features = [temp_features Features(:,features_remaining(i))];
        if logloss
            [~,error] = kFoldValidation(temp_temp_features, Label, C, sigma, ends);
        else
            error = kFoldValidation(temp_temp_features, Label, C, sigma, ends);
        end
        errors = [errors error];
    end
    best_error = min(errors);
    to_be_added = find(errors == best_error); % the feature we actually add is the one that led to the smallest validation error
    to_be_added = to_be_added(1); % in case of a tie
    
    if best_error < error_min
        error_min = best_error;
        temp_features = [temp_features Features(:, features_remaining(to_be_added))];
        features_added = [features_added features_remaining(to_be_added)];
        features_remaining(to_be_added) = [];
    else
        break;
    end
end

misclassification = error_min;

time_elapsed = toc / 60;

end

