#!usr/bin/perl

use warnings;
use strict;

# the idea of this program is to create both the MatLab program which calls the "kfold" program, and 
# create the slurm batch file, which is then run.
# The idea is that since parallelization isn't working, I could simply submit the job to multiple CPUs individually using a "splitter"
# 28-10-2019, MH: Edited to automatically update number of cell lines BUT need correct Features.Numerical-Matrix.txt file to be in the folder (program counts number of lines in this file). This file should be copied into the folder by the main pipeline program.

my $typeofFS = "FFS";  # BFS or FFS, currently CSFS doesn't work
my $druglabel = "Label.txt";
my $drugfeature = "Features.Numerical-Matrix.txt";

my @looping = ("1-1", "10-1", "100-1", "1000-1", "10000-1", "100000-1", "10-10", "100-10", "1000-10", "10000-10", "100000-10", "100-100", "1000-100", "10000-100", "100000-100", "1000-1000", "10000-1000", "100000-1000", "10000-10000", "100000-10000", "100000-100000");
my $numCellLines;

#counts number of lines (which is equal to the number of cell lines)
open NUMERICALFEATURES, "<Features.With-Numerical-Features.txt" or die $!;
while (<NUMERICALFEATURES>) {};
$numCellLines = $.;
close NUMERICALFEATURES;

my $outbatch = "Output.LogLoss.batchJobs.out"; #file to write sbatch job numbers to 
truncate $outbatch, 0; #clear file to start with clean file

foreach(@looping) {
	my @values = split ("-", $_);
	my $sigma = $values[1];
	my $Cval = $values[0];;

	# create script file	
	my $outfile = "$typeofFS\_LogLoss_K_Fold.SHARCNET-No-Parallel.$Cval.$sigma.m";
	open OUTPUT, "> $outfile" or die $!;

	print OUTPUT qq(LabelFile = fopen('$druglabel');\n);
	print OUTPUT qq(Label = fscanf(LabelFile,'%d');\n);
	print OUTPUT qq(FeaturesFile = fopen('$drugfeature');\n);
	print OUTPUT qq(sizeA = [$numCellLines Inf];\n);
	print OUTPUT qq(Features = fscanf(FeaturesFile,'%f',sizeA);\n);
	print OUTPUT qq(fclose(LabelFile);\n);
	print OUTPUT qq(fclose(FeaturesFile);\n);
	print OUTPUT qq([ features_added, misclassification ] = $typeofFS\_strat_kfold( Features, Label, 1:$numCellLines, $Cval, $sigma, true);\n);
	print OUTPUT qq(fileFeatures = fopen('Features_$typeofFS.LogLoss.$Cval-$sigma.txt','w');\n);
	print OUTPUT qq(fprintf(fileFeatures, 'C\\tSigma\\t\\tError\\tGenesPicked\\n');\n);
	print OUTPUT qq(fprintf(fileFeatures, '$Cval\\t$sigma\\t');\n);
	print OUTPUT qq(fprintf(fileFeatures, '%f\\t', misclassification);\n);
	print OUTPUT qq(fprintf(fileFeatures, '%d\\t', features_added);\n);
	print OUTPUT qq(fprintf(fileFeatures, '\\n');\n);
	print OUTPUT qq(fclose(fileFeatures);\n);

	close OUTPUT;



	# create batch file
	my $batchfile = "matlab-batch.$typeofFS.$Cval.$sigma.sh";

	open BATCH, "> $batchfile" or die $!;

	print BATCH qq(#!/bin/bash\n);
	print BATCH qq(#SBATCH --ntasks=1                # number of tasks\n);
	print BATCH qq(#SBATCH --cpus-per-task=1         # number of cores per task (adjust this when using PCT)\n);
	print BATCH qq(#SBATCH --mem-per-cpu=4096M       # memory; default unit is megabytes\n);
	print BATCH qq(#SBATCH --time=7-00:00            # time (DD-HH:MM)\n);
	print BATCH qq(#SBATCH --account=def-progan\n);
	print BATCH qq(#SBATCH --input=$outfile     # input file\n);
	print BATCH qq(#SBATCH --output=$outfile.log  # output file\n);
	print BATCH qq(module load matlab/2018a\n);
	print BATCH qq(srun matlab -nodisplay -nosplash -nojvm -singleCompThread\n);

	close BATCH;
	system("sbatch --parsable $batchfile >> $outbatch");
    print("sbatch --parsable $batchfile >> $outbatch\n");

	sleep(5);

	system ("rm $batchfile"); # this should be fine

}


