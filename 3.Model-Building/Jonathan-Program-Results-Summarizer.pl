#!usr/bin/perl

use warnings;
use strict;

# this program will read in all result files (Features_FFS.Misclass.1-1.txt, Features_FFS.Misclass.10-1.txt, etc)
# Summarize it into one file
# Alter column numbers to gene names

open GENES, "<Genes.txt" or die $!;

my %genes;

while (<GENES>) {
	chomp;
	my @columns = split ("\t", $_);

	my $count = 1;

	foreach(@columns) {
		my $genename = $_;
		$genes{$count} = $genename;
		$count++;
	}






}
close GENES;

my @looping = ("1-1", "10-1", "100-1", "1000-1", "10000-1", "100000-1", "10-10", "100-10", "1000-10", "10000-10", "100000-10", "100-100", "1000-100", "10000-100", "100000-100", "1000-1000", "10000-1000", "100000-1000", "10000-10000", "100000-10000", "100000-100000");

my @fileloop = ("Features_FFS.Misclass", "Features_FFS.LogLoss", "Features_BFS.Misclass", "Features_BFS.LogLoss");

print "C\tSigma\tError\tGenesPicked\n";

open OUTPUT, ">Annotated-And-Organized-Results.txt" or die $!;


foreach (@fileloop) {
	my $runtype = $_;

	print OUTPUT "Run: $runtype\n";
	my $Csigma;

	foreach $Csigma (@looping) {

		my $filename = "$runtype.$Csigma.txt";
		
		open FILE, "< $filename" or next;

		while (<FILE>) {
			chomp;
			my @columns = split ("\t", $_);
			next if ($_ =~ /^C/);	
			# 1       1       0.025641        1254
			my $Cval = shift (@columns);
			my $sigma = shift (@columns);
			my $misclass = shift(@columns);

			print OUTPUT "$Cval\t$sigma\t$misclass\t";
			foreach(@columns) {
				my $genename = $genes{$_};
				print OUTPUT "$genename\t";
			}	
	
		}

		close FILE;
		print OUTPUT "\n";
	}
	print OUTPUT "\n";

}

close OUTPUT;
