function [ num_features_final, min_error, time_elapsed ] = CSFS_strat_kfold( Features, Label, ends, ranking, C, sigma, logloss )
%CSFS_mRMR_SVM_MULTICLASS implements a complete sequential feature selection using a 
%SVM wrapper whose performance is evaluated by multi-class log loss on k-fold
%validation; the order in which features are added is specified by a mRMR ranking
%   Features is a M x N matrix of m samples and n features
%   Label is a M x 1 vector of target variables (one for each sample)
%   ends is a vector that marks the end of each fold
%   ranking is the 1 x N mRMR ranking of features (using either MID or MIQ criterion)
%   num_features_final is the number of features used by the best model
    % actual features can be found by passing this number into the
    % best_features function
%   min_error is the percent misclassification of the best model
%   C and sigma are the parameters used for building the SVM

tic;

errors = [];

temp_features = Features(:, ranking(1)); % first feature to be added is first one specified by ranking
next = 2; % next feature to be added is ranking(2)
% use LOOCV to evaluate performance of a linear regression model built from
% temp_features and Label
if logloss
    [~,error] = kFoldValidation(temp_features, Label, C, sigma, ends);
else
    error = kFoldValidation(temp_features, Label, C, sigma, ends);
end

errors = [errors error];

num_features = size(ranking,2);

% while there are still features to add, add feature and calculate LOOCV
% error
while size(temp_features,2) < num_features
    temp_features = [temp_features Features(:, ranking(next))];
    next = next + 1;
    if logloss
        [~,new_error] = kFoldValidation(temp_features, Label, C, sigma, ends);
    else
        new_error = kFoldValidation(temp_features, Label, C, sigma, ends);
    end
    errors = [errors new_error];
end

min_error = min(errors);
num_features_final = find(errors == min_error);

time_elapsed = toc / 60;

end

