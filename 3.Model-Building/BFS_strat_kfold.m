function [ remaining, error_min, misclassifications, time_elapsed ] = BFS_strat_kfold( Features, Label, ends, C, sigma, logloss )
%BFS_SVM_MULTICLASS Summary of this function goes here
%BFS_BCDT implements sequential backward feature selection using a decision
%tree wrapper whose performance is evaluated by LOOCV
%   Features is a M x N matrix of m samples and n features
%   Label is a M x 1 vector of target variables (one for each sample)
%   ends is a vector that marks the end of each fold

tic

%Features = zscore(Features);
k = size(Features,1) * 0.2; % 20% of samples will be left out at a time during validation

temp_features = Features; % start with all features
% use LOOCV to evaluate performance of a linear regression model built from
% temp_features and Label
if logloss
    [~,error_min] = kFoldValidation(temp_features, Label, C, sigma, ends); % this error value is the temporary minimum that needs to be "beat"
else
    [error_min] = kFoldValidation(temp_features, Label, C, sigma, ends); % this error value is the temporary minimum that needs to be "beat"
end
misclassifications = [error_min]; % keep track of all errors for graphing purposes
remaining = 1:size(Features,2); % keeps track of features remaining

% while there are still features to remove, try removing all possible features, calculate new LSE and
% compare with previous LSE
while size(temp_features,2) > 1
    errors = [];
    % i is the feature to be removed
    parfor i=1:size(temp_features,2)
        temp_temp_features = temp_features;
        temp_temp_features(:,i) = []; % remove feature i
        %error = SVM_multiclass_LOOCV(temp_temp_features, Label, C, sigma);
        if logloss
            [~,error] = kFoldValidation(temp_temp_features, Label, C, sigma, ends); % this error value is the temporary minimum that needs to be "beat"
        else
            [error] = kFoldValidation(temp_temp_features, Label, C, sigma, ends); % this error value is the temporary minimum that needs to be "beat"
        end
        errors = [errors error];
    end
    best_error = min(errors);
    to_be_removed = find(errors == best_error); % the feature we actually remove is the one that led to the smallest validation error
    to_be_removed = to_be_removed(1); % in case of tie
    
    if best_error < error_min
        error_min = best_error;
        temp_features(:,to_be_removed) = []; % remove feature permanently
        remaining(to_be_removed) = [];
        misclassifications = [misclassifications error_min];
    else
        break;
    end
    
end

time_elapsed = toc / 60;

end

