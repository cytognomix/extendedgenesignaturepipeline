#   Author: Michelle Ho
#   Algorithm:
#   Remove control-phosphorylation-of and CHEBI:
#   Forget directions and group gene to gene links
#   Collect genes i.e. A -> [B,C,D] || B -> [A.R]
#   Find next level for subset i.e. [B,C,D], repeat until come across GOI
#   Return how far away from the query gene the GOI is
#
# 2020-03-03 edit: use PathwayCommons12.All.hgnc.txt (includes pathway name for some genes),
# exit loop when gene distance > 2
#library(dplyr)

query_gene_distance <- function(row, table_mydata_filtered){
  #check that both genes exist in table
  sink("output.txt", append = TRUE)
  query_gene <- row[1]
  query_GOI <- row[2]
  cat ("\n")
  cat ("PAIR: ", query_gene, "\t", query_GOI, "\n")
  
  set_genesR <- table_mydata_filtered$PARTICIPANT_B
  set_genesL <- table_mydata_filtered$PARTICIPANT_A
  if (!(is.element(query_gene, set_genesR)) || !(is.element(query_gene, set_genesL))){
    cat ("Does not exist: ", query_gene, "\n")
    cat("GENE DISTANCE: ", -1, "\n")
    sink()
    return(-1)
  }
  
  if (!(is.element(query_GOI, set_genesR)) || !(is.element(query_GOI, set_genesL))){
    cat ("Does not exist: ", query_GOI, "\n")
    cat("GENE DISTANCE: ", -1, "\n")
    sink()
    return(-1)
  }
  
  gene_distance <- 0
  list_steps <- list()
  list_backtrack <- list()
  
  query_table <- subset(table_mydata_filtered, PARTICIPANT_A == query_gene) #node0 gene relations
  query_table2 <- subset(table_mydata_filtered, PARTICIPANT_B == query_gene) #node0, where query_gene is on R side of assignment
  
  #remove genes already put into query_table so no overlap
  query_table_filtered <- subset(table_mydata_filtered, PARTICIPANT_A != query_gene)
  query_table_filtered <- subset(table_mydata_filtered, PARTICIPANT_B != query_gene)

  #Interactions where query_gene is on right side of assignment, flip assignment so query_gene is on left side
  c <- query_table2$PARTICIPANT_B
  a <- query_table2$PARTICIPANT_A
  
  query_table2$PARTICIPANT_A = c
  query_table2$PARTICIPANT_B = a
  
  query_table <- rbind(query_table, query_table2)
  #list_steps[[0]] <- query_gene #add step01 to list for backtracking
  list_steps[[1]] <- query_table #add step 1 to list for backtracking
  ##END assignment flip
  
  set_node <- query_table$PARTICIPANT_B #node 1
  set_node[!duplicated(set_node)] #make sure there are no duplicates
  #print(set_node)
  gene_distance = gene_distance + 1 #gene_distance = 1
  if (is.element(query_GOI, set_node)){
    cat("GENE DISTANCE: ", gene_distance, "\n")
    sink()
    return (gene_distance)
  }
  
  while(TRUE){
    query_table <- subset(query_table_filtered, is.element(PARTICIPANT_A, set_node)) #find step x genes on left assignment
    query_table_filtered <- subset(query_table_filtered, !is.element(PARTICIPANT_A, set_node)) #remove step x genes on left assignment in filtered table
    set_node <- query_table$PARTICIPANT_B #set of step(x+1) genes

    gene_distance = gene_distance + 1 #step x
    list_steps[[gene_distance]]<- query_table ##add step x to list for backtracking
    if (is.element(query_GOI, set_node)){
      #print("GENE FOUND")
      break
    } else if (gene_distance >= 2) {
      cat ("Gene distance greater than 2: ", query_gene, "\n")
      sink()
      return(-1)
    }
    
  }

  #backtrack to find order of nodes to GOI
  ctr <- gene_distance
  list_backtrack[[ctr]] <- subset(list_steps[[ctr]], PARTICIPANT_B == query_GOI)
  set_node = list_backtrack[[ctr]]$PARTICIPANT_A
  ctr = ctr - 1
  while(ctr>0){
    list_backtrack[[ctr]] <- subset(list_steps[[ctr]], is.element(PARTICIPANT_B, set_node))
    set_node = list_backtrack[[ctr]]$PARTICIPANT_A
    ctr = ctr - 1
  }

  #sprint("BACKTRACKING")
  print(list_backtrack)
  
  cat("GENE DISTANCE: ", gene_distance, "\n")
  
  sink()
  return (gene_distance)
}

#args = commandArgs(trailingOnly=TRUE)
#print(args)

mydata <- read.table("C:/Users/Michelle Ho/Desktop/Year 4/Thesis projects/Peter Rogan/Code/PathwayCommons12.All.hgnc.txt", header = TRUE, sep = "\t", fill=TRUE)
mydata_filtered <- subset(mydata, INTERACTION_TYPE != "controls-phosphorylation-of")
mydata_filtered <- subset(mydata_filtered, !grepl("CHEBI", PARTICIPANT_B))
mydata_filtered <- subset(mydata_filtered, !grepl("CHEBI", PARTICIPANT_A))

gene_pairs_table <- read.table("C:/Users/Michelle Ho/Desktop/Year 4/Thesis projects/Peter Rogan/Code/pathwayRelations_genePairs.txt", header = TRUE, sep = "\t", fill=TRUE)

apply(gene_pairs_table, 1, query_gene_distance, table_mydata_filtered = mydata_filtered)
#query_gene_distance(args[[1]], args[[2]], mydata_filtered)

