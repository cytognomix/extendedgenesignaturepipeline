MFA?	no
SVM gene filtering?	no
Model Building?	no
Probe Finding?	yes
Model Testing?	yes

(1)	MFA
drug		TESTDRUG
genesInitial	ABCC10	BCL2	BCL2L1	BIRC5	BMF	FGF2	FN1	MAP4	MAPT	NFKB2	SLCO1B3	TLR6	TMEM243	TWIST1	CSAG2
aliasesFile	SVM-Modelling-Project/GeneNames-Association.Pseudonyms.txt
relationsFile	SVM-Modelling-Project/PathwayCommons.OneNode.InteractionFile.txt.sif
gi50sFile	SVM-Modelling-Project/GI50-Data.txt
copiesFile	SVM-Modelling-Project/CopyNumber-Data.txt
expressionsFile	SVM-Modelling-Project/GeneExpression-Data.txt
angleCutoff	70
stepsCutoff	2
circleOutput	False
mfaInput        False
mfaOutput       True
svmInput        True
aliasOutput	True

(2)	SVM gene filtering
step0	30	20
step1	20	0
step2	10	0
Increment by	5
LabelFile	18.tsv	#needs to be downstream of SVM directory

(3)	Model Building
Misclassification?	yes
Logloss?	yes/no

(4)	Probe Finding
Platform File	GPL96-57554.txt
Series File	GSE25066_series_matrix.Taxol-Only.txt

(5)	Model Testing
Model	Model.txt
TrainingData	18.tsv
PatientFeatures	PatientGE.tsv
PatientLabel	Label.tsv	#Note: pre-make Label.tsv file from patient data, place in main folder
SkippedGenes	Output.Skipped-genes.txt
