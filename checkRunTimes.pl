=pod
@author: Michelle Ho
@date: 2019-11-26

Program reads job numbers for model building and/or model testing and runs seff for each job to obtain elapsed time.
Program writes separate file with just job times; use to peform regression analysis
=cut

use warnings;
use strict;

my $path_MB = "3.Model-Building";
my $path_MT = "5.Model-Testing";
my $file_jobs = "Output.Misclassification.batchJobs.out";
my $file_output = "Log.".$file_jobs.".log";

open CONFIG, "<pipeline_config.txt" or die $!;
my $step0;
my $step1;
my $step2;
my $increment;
my $labelFile;
my ($step0_start, $step1_start, $step2_start);
my ($step0_end, $step1_end, $step2_end);

#extract information from pipeline_config.txt
$start = 0;
while (<CONFIG>){
    if (($start eq 1) && ($_ eq "\n")) {
        $start = 0;
        last;
    }

    if ($start eq 1){
        chomp;
        my @columns = split ("\t", $_);
        my $descript = $columns[0];
        my $result = $columns[1];
        if ($descript eq "step0") {
            $step0 = $step0_start = $result;
            $step0_end = $columns[2];
        } elsif ($descript eq "step1") {
            $step1 = $step1_start = $result;
            $step1_end = $columns[2];
        } elsif ($descript eq "step2") {
            $step2 = $step2_start = $result;
            $step2_end = $columns[2];
        } elsif ($descript eq "Increment by") {
            $increment = $result;
        }elsif ($descript eq "LabelFile") {
            $labelFile = $result;
        }
    }

    if ($_ eq "(2)\tSVM gene filtering\n") { $start = 1;}
}

close CONFIG;

open OUTFILE, ">$file_output" or die $!;

my $shell_parameters;
while ($step0 >= $step0_end){
    $step1 = $step1_start;
    while ($step1 >= $step1_end){
        $step2 = $step2_start;
        while ($step2 >= $step2_end){
            #runPipeline();
            my $path_parameters = $step0."deg0-".$step1."deg1-".$step2."deg2";
            my $path_to_file = $path_parameters."/".$path_MB."/".$file_jobs;
            
            open JOBSFILE, "<$path_to_file" or die $!;
            while (<JOBSFILE>){
                chomp;
                my $status = `seff $_`;
                print OUTFILE "$status\n";
            }
            close JOBSFILE;
            $step2 = $step2 - $increment;
        }
        
        $step1 = $step1 - $increment;   
    }
    $step0 = $step0 - $increment;    
}

close OUTFILE;

open OUTFILE, "<$file_output" or die $!;
open OUTTIMES, ">CPU_usage_time_$file_jobs.log" or die $!;
my @times;
while (<OUTFILE>){
    chomp;
    my @columns = split (": ", $_);
    my $descript = $columns[0];
    my $result = $columns[1];

    if ($descript eq "CPU Utilized") {
        push(@times, $result);
        print OUTTIMES $result;
    }
}
close OUTFILE;

my $length = @times;
my $sum = 0;
foreach (@times){
    $sum += $_;
}
my $avg = $sum / $length;
print "Average time: $avg";




