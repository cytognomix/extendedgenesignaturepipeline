#!usr/bin/perl

use warnings;
use strict;

# this program wants to take the daemen data (Cisplatin-20angle-20.tsv) 
# and convert it to a readable format for Jonathan's patient testing program "regularValidation_multiclassSVM.m"
#
# So what I need to do is
# 1. Strip the first column and second columns
# 2. Seperate the second column into it's own file ("Label") (skip first line)
# 3. Print the rest (do NOT skip first line)

my $file = $ARGV[0];


open FILE, "< $file" or die $!;

open OUTLABEL, ">Training-Label.tsv" or die $!;
open OUTFEATURE, ">Training-Features.tsv" or die $!;

my $loopcount = 0;

while (<FILE>) {
	chomp;
	my @columns = split ("\t", $_);

	my $cellname = shift(@columns);
	my $GI50 = shift(@columns);
	my $label = shift(@columns);
	
	print OUTLABEL "$label\n" unless ($loopcount == 0);

	my $foreachloop = 0;

	foreach(@columns) {
		if ($foreachloop == 0) {
			print OUTFEATURE "$_";
		} else {
			print OUTFEATURE "\t$_";
		}
		$foreachloop++;
	}

	print OUTFEATURE "\n";


	$loopcount++;
}

close FILE;

close OUTLABEL;
close OUTFEATURE;
