#!usr/bin/perl
use warnings;
use strict;

# this program will read files which are needed to build models
# it will then test them against a modified version of Jonathan's program regularValidation_multiclassSVM.m
# and present accuracy measurements in the file Model-to-Patient-Data-Comparison-Results.txt
#
#

# Files.txt should be a tab delimited file which describes the 
open CONFIG, "<Required-Files.txt" or die $!;

my $model;
my $trainingdata;
my $patientfeatures;
my $patientlabel;
my $skippedGenes;

my $found = 0;

while (<CONFIG>) {

	chomp;
	my @columns = split ("\t", $_);

	my $descript = $columns[0];
	my $file = $columns[1];

	if ($descript eq "Model") {
		$model = $file;
		$found++;
	} elsif ($descript eq "TrainingData") {
		$trainingdata = $file;
		$found++;
	} elsif ($descript eq "PatientFeatures") {
		$patientfeatures = $file;
		$found++;
	} elsif ($descript eq "PatientLabel") {
		$patientlabel = $file;
		$found++;
	} elsif ($descript eq "SkippedGenes") {
		$skippedGenes = $file;
		$found++;
	}


}

close CONFIG;

# check to see if we have the information we require
if ($found != 5) {
	print "Please check Files.txt. It seems as a required file was not included.\n";
	die;
}

### we need to split the 



system ("perl Drug-to-Daemen-Data-Converter.pl $trainingdata");
my $celllinelabel = "Training-Label.tsv";
my $celllinefeatures = "Training-Features.tsv";


# now I need to get the XY dimensions of $celllinefeatures
open CELLFEATURE, "< $celllinefeatures" or die $!;

my $celllinefeatureX;

while (<CELLFEATURE>) {
	chomp;
	my @columns = split ("\t", $_);

	$celllinefeatureX = scalar(@columns);
}
close CELLFEATURE;


# now we get patient features
open PATIENTFEATURE, "< $patientfeatures" or die $!;

my $patientfeatureX;

while (<PATIENTFEATURE>) {
        chomp;
        my @columns = split ("\t", $_);

	$patientfeatureX = scalar(@columns);
}
close PATIENTFEATURE;

# here we grab the genes to skip (genes with no probes)
open SKIPPEDGENES, "< $skippedGenes" or die $!;

my @genesToSkip;

while (<SKIPPEDGENES>) {
	chomp;
	@genesToSkip = split("\t", $_);
}

my %hash_genesToSkip = map {$_ => 1} @genesToSkip;

close SKIPPEDGENES;

# here we grab the C and sigma value of the model,
open MODEL, "< $model" or die $!;
my $Cvalue;
my $sigmavalue;

my $numberofmodels = 0;

my $outbatch = "Output.ModelTesting.batchJobs.out"; #file to write sbatch job numbers to
open OUTBATCH, "> $outbatch" or die $!; #open file if it doesn't exist yet
close OUTBATCH;
truncate $outbatch, 0; #clear file to start with clean file

while (<MODEL>) {
	chomp;
	my @columns = split ("\t", $_);
	my $length = @columns;
	if ($length eq 1) { next;}
	$Cvalue = shift(@columns);
	$sigmavalue = shift(@columns);
	shift(@columns); #skip misclassification score

	my $count = 0;
	$numberofmodels++;

	my $outmodel = "Out-Model.$numberofmodels.txt";

	open OUTMODEL, "> $outmodel" or die $!;

	#debugging
	#foreach my $key (keys %hash_genesToSkip){
		#print "Hashkey: $key \n";
	#}

	foreach(@columns) {

		if(exists($hash_genesToSkip{$_})) {
			print "Delete: $_ \n";
			next;
		} #skip genes with no probes
		else {
			print "$_ \n";
		}

        if ($count == 0) {
	                print OUTMODEL "$_";
	    } else {
	                print OUTMODEL "\t$_";
	    }
        $count++;
    }
    
	print OUTMODEL "\n";

	close OUTMODEL;


	my $modelsize = $count;




	# now we want to write the code

	my $codefile = "Run-On-SHARCNET.regularValidation_multiclassSVM.Automated.$numberofmodels.m";

	open CODE, "> $codefile" or die $!;


	# Section gathering Cell Line data
	print CODE qq(% Cell Line Data\n\n);
	print CODE qq(CellLineLabelFile = fopen\('$celllinelabel'\);\n);
	print CODE qq(CellLineLabel = fscanf\(CellLineLabelFile,'%d'\);\n);
	print CODE qq(CellLineFeaturesFile = fopen\('$celllinefeatures'\);\n);
	print CODE qq(opt = {'Delimiter','\\t', 'CollectOutput',true};\n);
	print CODE qq(numgenes = $celllinefeatureX;\n);
	print CODE qq(fmt = '';\n);
	print CODE qq(counter = 0;);
	print CODE qq(for i = 1:numgenes\n);
	print CODE qq(\ts2 = '%s';\n);
	print CODE qq(\tfmt = strcat\(fmt,s2\);\n);
	print CODE qq(\tcounter = counter + 1;\n);
	print CODE qq(end\n);
	print CODE qq(CellLineFeatures = textscan\(CellLineFeaturesFile,fmt,opt{:}\);\n);
	print CODE qq(CellLineFeatures = CellLineFeatures{1};\n);
	print CODE qq(fclose\(CellLineLabelFile\);\n);
	print CODE qq(fclose\(CellLineFeaturesFile\);\n\n);

	# Section gathering patient data
	print CODE qq(% Patient Data\n\n);
	print CODE qq(PatientLabelFile = fopen\('$patientlabel'\);\n);
	print CODE qq(PatientLabel = fscanf\(PatientLabelFile,'%d'\);\n);
	print CODE qq(PatientFeaturesFile = fopen\('$patientfeatures'\);\n);

	print CODE qq(opt = {'Delimiter','\\t', 'CollectOutput',true};\n);
	print CODE qq(numgenes = $patientfeatureX;\n);
	print CODE qq(fmt = '';\n);
	print CODE qq(counter = 0;);
	print CODE qq(for i = 1:numgenes\n);
	print CODE qq(\ts2 = '%s';\n);
	print CODE qq(\tfmt = strcat\(fmt,s2\);\n);
	print CODE qq(\tcounter = counter + 1;\n);
	print CODE qq(end\n);
	print CODE qq(PatientFeatures = textscan\(PatientFeaturesFile,fmt,opt{:}\);\n);
	print CODE qq(PatientFeatures = PatientFeatures{1};\n);
	print CODE qq(fclose\(PatientLabelFile\);\n);
	print CODE qq(fclose\(PatientFeaturesFile\);\n\n);

	# section gathering model data
	print CODE qq(% Model Data\n);
	print CODE qq(C_value = $Cvalue;\n);
	print CODE qq(sigma_value = $sigmavalue;\n);
	print CODE qq(ModelFile = fopen\('$outmodel'\);\n);
	print CODE qq(opt = {'Delimiter','\\t', 'CollectOutput',true};\n);
	print CODE qq(numgenes = $modelsize;\n);
	print CODE qq(fmt = '';\n);
	print CODE qq(counter = 0;);
	print CODE qq(for i = 1:numgenes\n);
	print CODE qq(\ts2 = '%s';\n);
	print CODE qq(\tfmt = strcat\(fmt,s2\);\n);
	print CODE qq(\tcounter = counter + 1;\n);
	print CODE qq(end\n);
	print CODE qq(ModelData = textscan\(ModelFile,fmt,opt{:}\);\n);
	print CODE qq(ModelData = ModelData{1};\n);
	print CODE qq(fclose\(ModelFile\);\n\n);
	print CODE qq(% Main Program\n);
	print CODE qq([ mis, mis_err, logloss, logloss_err, gof, gof_err, predictions_arr, misclasses_bd, misclasses_bd_err,hyperplane_arr ] = regularValidation_multiclassSVM_SHARCNET\(ModelData, C_value, sigma_value, CellLineFeatures, CellLineLabel, PatientFeatures, PatientLabel, 0\);\n\n);

	print CODE qq(% Output Code\n);
	## now we must grab the output. mis, mis_err, logloss, logloss_err, gof, gof_err are all single numbers
	# misclasses_bd and Missclass_bd_err are two values
	# and predictions_arr is the complicated one, it's 10 cells of prediction values
	# I might just skip that one for now

	my $modelout = "Model-to-Patient-Data-Comparison-Results.$numberofmodels.txt";

	print CODE qq(fileFeature = fopen\('$modelout','w'\);\n);
	print CODE qq(fprintf\(fileFeature, 'Misclass\\tMisclass_Error\\tLogLoss\\tLogLoss_Error\\tGOF\\tGOF_Error\\tAccuracy0\\tAccuracy1\\tAccuracy0_err\\tAccuracy1_err\\n'\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t', mis\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t', mis_err\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t', logloss\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t', logloss_err\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t', gof\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t', gof_err\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t%f\\t', misclasses_bd\);\n);
	print CODE qq(fprintf\(fileFeature, '%f\\t%f\\t', misclasses_bd_err\);\n);
#	print CODE qq(fprintf\(fileFeature, '%d\t', predictions_arr\)\n);
	print CODE qq(celldisp\(predictions_arr\);\n);
        print CODE qq(celldisp\(hyperplane_arr\);\n);
		
	print CODE qq(fclose\(fileFeature\);\n);

	close CODE; 

	# make the shell script to run the matlab script


	my $shellfile = "shell-script.$numberofmodels.sh";
	open SHELL, "> $shellfile" or die $!;
	print SHELL qq(#!/bin/bash\n);
	print SHELL qq(#SBATCH --mem-per-cpu=4096M\n);
	print SHELL qq(#SBATCH --time=0-00:10\n);
	print SHELL qq(#SBATCH --account=def-progan\n);
	print SHELL qq(#SBATCH --input=$codefile\n);
	print SHELL qq(#SBATCH --output=$codefile.log\n);
	print SHELL qq(module load matlab/2019a\n);
	print SHELL qq(srun matlab -nodisplay -nosplash\n);
	print SHELL qq(rm $shellfile\n);

	close SHELL;

	system("sbatch --parsable $shellfile >> $outbatch");
    print("sbatch --parsable $shellfile >> $outbatch\n"); #MH: print job number to outbatch file to monitor job status by main program

	sleep(1)

}
close MODEL;

