#!usr/bin/perl

use warnings;
use strict;

# this program must perform the following
# find all the expected output files from Perl-Shell.regularValidation_multiclassSVM.pl
#
#

#open OUTPUT, ">output.Regular-Validation-Program-Output-Organizer.By-Patient.txt" or die $!;
open SUMMARIZE, ">output.Regular-Validation-Program-Output-Organizer.Summarized.txt" or die $!;

print SUMMARIZE "Model\tMisclass\tMisclass_Error\tLogLoss\tLogLoss_Error\tGOF\tGOF_Error\tAccuracy0\tAccuracy1\tAccuracy0_err\tAccuracy1_err\tMCC\n";

# get patent label: we go through "Required-Files.txt" in case the filename isn't Label.txt

open REQUIRED, "<Required-Files.txt" or die $!;

my $count = 1;
my %labelhash;
my $numbersensitive = 0;
my $numberresistant = 0;

while (<REQUIRED>) {
	chomp;
	my @columns = split("\t", $_);
	my $labelfile;

	# PatientLabel    Label.tsv
	if ($columns[0] eq "PatientLabel") {
		$labelfile = $columns[1];

		open LABEL, "< $labelfile" or die $!;

		while (<LABEL>) {
			chomp;

			$labelhash{$count} = $_;
			if ($_ == 0) {
				$numbersensitive++;
			} elsif ($_ == 1) {
				$numberresistant++;
			} else {
				print "There is a non-0/1 in the label file...\n";
				die;
			}
	

			$count++;
		}
		last;
	}

}
close REQUIRED;


my $i;

for ($i = 1; $i < 1000; $i++) {

#Model-to-Patient-Data-Comparison-Results.1.txt

#		my $zeroprog = "Run-On-SHARCNET.regularValidation_multiclassSVM.Automated.$geneofinterest.0-stddev.m";
#		system ("rm $zeroprog");

		my %zeropredict;

		my $zeroname = "Run-On-SHARCNET.regularValidation_multiclassSVM.Automated.$i.m.log";
		open ZERO, "< $zeroname" or next;

		my $processflag = 0;
		my $zeroloopcount = 1;

		while (<ZERO>) {

			chomp;
			my @pieces = split ("\t", $_);

			if ($_ =~ /predictions_arr/) {

				$processflag = 1;
				next;
			} elsif ($_ =~ /hyperplane_arr/) {
				$processflag = 0;
			}

			next if ($processflag == 0);
			if ($_ =~ /\d+/) {
				if (exists $zeropredict{$zeroloopcount}) {				
					$zeropredict{$zeroloopcount} += $_;

				} else {
					$zeropredict{$zeroloopcount} = $_;
				}


				$zeroloopcount++;
				
			} else {
				$zeroloopcount = 1;

			}



		}
		close ZERO;



		# we also want to summarize the overall accuracy files
		# Model-to-Patient-Data-Comparison-Results.1.txt
		my $accuracyfile = "Model-to-Patient-Data-Comparison-Results.$i.txt";

		open ACCURACY, "< $accuracyfile" or die $!;

		# number sensitive, number resistant
		while (<ACCURACY>) {
			chomp;
			my @chunks = split ("\t", $_);
			next if ($_ =~ /Misclass/);

			my $accuracy0 = $chunks[6];
			my $accuracy1 = $chunks[7];

			my $TP = $accuracy0*$numbersensitive;
			my $FP = $numbersensitive - $TP;
			my $TN = $accuracy1*$numberresistant;
			my $FN = $numberresistant - $TN;

			my $MCCnumerator = ($TP*$TN) - ($FP*$FN);
			my $MCCdenominator = sqrt(($TP+$FP) * ($TP+$FN) * ($TN+$FP) * ($TN+$FN));
			my $MCC;
			
			if ($MCCdenominator == 0) {
				$MCC = 0;
			} else {
				$MCC = $MCCnumerator/$MCCdenominator;
			}

			print SUMMARIZE "$i\t$chunks[0]\t$chunks[1]\t$chunks[2]\t$chunks[3]\t$chunks[4]\t$chunks[5]\t$chunks[6]\t$chunks[7]\t$chunks[8]\t$chunks[9]\t$MCC\n";



		}

		close ACCURACY;







}



#close OUTPUT;
close SUMMARIZE;

