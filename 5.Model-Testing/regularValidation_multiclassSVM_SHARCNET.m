function [ mis, mis_err, logloss, logloss_err, gof, gof_err, predictions_arr, misclasses_bd, misclasses_bd_err,hyperplane_arr ] = regularValidation_multiclassSVM_SHARCNET( signature, C, sigma, fea_tr, lab_tr, fea_te, lab_te, plot_hyperplane_dists )
%REGULARVALIDATION_MULTICLASSSVM this function takes in a signature (cell
%of strings), C (double), sigma (double), and feature (cell) and label
%(column vector) arrays; traditional validation is performed and
%performance metrics are calculated
%plot_hyperplane_dists is a boolean - 1 (TRUE) if you want to plot, 0
%otherwise (only works if the number of classes is 2)

num_genes = length(signature);

% extract genes of signature from fea_te
features_te = [];
for i=1:num_genes
    gene = signature{i};

    % fea_test
    found = false;
    for k=1:size(fea_te,2)

    	% EJM - added code, the old comparison was leading to a "matrix comparison" issue, this is a more "accepted" way of comparing two strings, and it works
    	uppergene = upper(gene);
	upperdata = upper(fea_te{1,k});

	if strcmpi(uppergene, upperdata)
%        if upper(gene) == upper(fea_te{1,k})
	% EJM - program was considering the gene expression / copy number as a string, which made cell2mat fail. This way avoids this issue.
	    Feature_temp = fea_te(2:size(fea_te,1),k);
            Train = str2double(Feature_temp);
	    features_te = [features_te Train];

%            features_te = [features_te cell2mat(fea_te(2:size(fea_te,1),k))];
            found = true;
            break;
        end
    end
    if ~found
        upper(gene)
        error("Gene not found in test set");
    end
end

% extract genes of signature from fea_tr
features_tr = [];
for i=1:num_genes
    gene = signature{i};
    
    % fea_test
    found = false;
    for k=1:size(fea_tr,2)
	uppergene = upper(gene);
	upperdata = upper(fea_tr{1,k});

	if strcmpi(uppergene, upperdata)	
		% EJM - same fixes in the section above are repeated here. These 4 lines are all that needed to be changed.
%	if upper(gene) == upper(fea_tr{1,k})
%           features_tr = [features_tr cell2mat(fea_tr(2:size(fea_tr,1),k))];
	    Feature_temp = fea_tr(2:size(fea_tr,1),k);
	    Train = str2double(Feature_temp);
	    features_tr = [features_tr Train];
            found = true;
            break;
        end
    end
    if ~found
        upper(gene)
        error("Gene not found in training set");
    end
end

num_samples_tr = size(features_tr,1);
num_samples_te = size(features_te,1);

% normalize by patient (only if we have more than 1 gene in the signature)
if num_genes > 1
    norm_features = quantilenorm([features_tr; features_te]')';
else
    norm_features = [features_tr; features_te];
end
features_tr_norm = norm_features(1:num_samples_tr,:);
features_te_norm = norm_features(num_samples_tr+1:num_samples_tr+num_samples_te,:);

misclasses = [];
loglosses = [];
gofs = [];

if unique(lab_tr) ~= unique(lab_te)
    error("Training labels do not match test labels")
end

classes = unique(lab_tr);
misclasses_bd = []; % this array will have m columns where m is the number of classes

n=10; predictions_arr = cell(10,1);
hyperplane_arr = cell(10,1);


for i=1:n
    hyperplane_distances = zeros(1,num_samples_te); % these "distances" only make sense in the case where there are 2 classes
    t = templateSVM('KernelFunction', 'rbf', 'BoxConstraint', C, 'KernelScale', sigma);
    svmModel = fitcecoc(features_tr_norm, lab_tr, 'learners', t, 'FitPosterior',1);
	[predictions,hyperplane,~,Posterior] = predict(svmModel,features_te_norm); % posterior contains probabilities of a sample falling under each class

    predictions_arr{i,1} = predictions;
    hyperplane_arr{i,1} = hyperplane;

    
    for k=1:length(classes)
        num_correct = 0;
        count = 0;
        for h=1:length(predictions)
            if lab_te(h) == classes(k)
                count = count + 1;
                if predictions(h) == lab_te(h)
                    num_correct = num_correct + 1;
                end
            end
        end
        misclasses_bd(i,k) = num_correct / count;
    end
    
    % loop through each sample and accumulate the log_loss
    log_loss = 0;
    for j=1:length(predictions)
        ind = find(svmModel.ClassNames == lab_te(j)); % find index of ClassNames for the correct class of this sample
        % find probability of sample being in the correct class
        prob = max(min(Posterior(j,ind),1-10^-15),10^-15);
        hyperplane_distances(1,j) = abs(prob-0.5); % update hyperplane_distances array
        log_loss = log_loss + log(prob);
    end
    log_loss = -log_loss / length(predictions);
    

    if (length(classes) == 2 && plot_hyperplane_dists)
        figure;
        scatter(1:length(predictions),hyperplane_distances);
        hold on;
    end
    
    % update num_correct and num_predictions
    mis = mean(predictions ~= lab_te);
    gof = mean(abs(predictions - lab_te));
    
    misclasses = [misclasses mis];
    loglosses = [loglosses log_loss];
    gofs = [gofs gof];
end

mis = mean(misclasses);
mis_err = std(misclasses)/sqrt(n);
logloss = mean(loglosses);
logloss_err = std(loglosses)/sqrt(n);
gof = mean(gofs);
gof_err = std(gofs)/sqrt(n);

misclasses_bd_err = std(misclasses_bd)/sqrt(n);
misclasses_bd = mean(misclasses_bd);

end

