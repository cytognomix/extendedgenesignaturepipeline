Model Building and Testing Pipeline
Author: Michelle Ho

Files included:
1. Pipeline_batchjobs.pl
	Reads pipeline_config.txt to run the program in batch files.
	Runs MFA if MFA is set to run in the config file.
	Submits each set of paramaters (e.g. 30deg0-20deg1-10deg2) as a batch job that runs RunPipeline.pl to allow the jobs to be run on multiple nodes.
2. RunPipeline.pl
	Program submitted to the batch job.
	Pipeline that calls programs for the SVM, model building, probe finding, and model testing steps.
	RunPipeline.pl checks to ensure model building step is complete before moving onto the next step (this step is also run in parallel).
	Job for that set of parameters dies if an error comes up.
3. checkRunTimes.pl
	Program used to determine how long each model building and testing job took to run.
4. SortByMCC.pl
	Run in Output_models directory after model testing is complete.
	Not yet incorporated into the pipeline because program was designed to make data visualization easier but easily could be.
	Amalgamates model testing results and sorts them in order of the absolute value of the MCC score.
	Retrieves the gene signature for each model; Gene signature is added to the amalgamated file (original model testing results include the model number but not the actual gene signature which had to be looked up manually).
5. pipeline_config.txt

User-inputed files required:
	Platform file for probe finding
	Series file for probe finding
	Patient label file for model testing

Directories/Files needed to run:
/1.PerformingMFA
	SVM-Modelling-Project directory
	All files unchanged, see folder
	
/2.SVM
	SVM-Gene-List-Filter-and-Printer-GEOnly.GENENAME-and-TAB-FIXED_edit.pl
/3.Model-Building
	Jonathan-Program-Results-Summarizer.pl
	MatLab-Script-Writer-for-Job-Split.Misclass_edit.pl
	MatLab-Script-Writer-for-Job-Split_edit.pl
	BFS_strat_kfold.m
	CSFS_strat_kfold.m
	FFS_strat_kfold.m
	kFoldValidation.m
/4.Probe-Finding
	Model-Probe-FinderGPL96_edit.pl
	Probe-ID-Averaging-Program_edit.pl
/5.Model-Testing
	Drug-to-Daemen-Data-Converter.pl
	Perl-Shell.regularValidation_multiclassSVM.Multiple-Model-Version_edit.pl
	Regular-Validation-Results-Program-Output-Organizer.pl
	regularValidation_multiclassSVM.m
	regularValidation_multiclassSVM_SHARCNET.m
	Required-Files.txt

Edits made in previously made program files:
	MatLab-Script-Writer-for-Job-Split.Misclass_edit.pl, MatLab-Script-Writer-for-Job-Split_edit.pl
		Edited to automatically update number of cell lines
		Print job numbers to and output file so job status can be monitored by the main program

	Model-Probe-FinderGPL96_edit.pl, Probe-ID-Averaging-Program_edit.pl
		Platform and series files are soft-coded. Names for these files are taken from the main program, which reads it from the user-inputted config file.
		Genes with no probes are recorded to a file so that the models file does not have to be editted manually for each run.

	Perl-Shell.regularValidation_multiclassSVM.Multiple-Model-Version_edit.pl
		Automatically skips over genes with no probes (in original program these would have to be removed manually). Skipped genes are recorded in a separate file for reference.
		Print job numbers to and output file so job status can be monitored by the main program