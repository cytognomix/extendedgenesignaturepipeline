=pod
Developer: Michelle Ho
Program: Amalgamate model testing results into 1 file, sort in order of decreasing MCC, and fetch additional information not included in model testing results:
    Genes in model
    C value
    Sigma Value

2020-03-06: Omit genes that were skipped during model testing because no probe information
=cut
my $file;
my @infoArray;

print "Starting\n";

my @files = glob("*Model_*");
my $files_length = @files;
print "\nNumber of files: $files_length\n";
my $model_ctr = 0;


foreach my $file (@files) {
    #$file = "Model_".$step0."deg0-".$step1."deg1-".$step2."deg2.txt";
    print $file;
    

    open MODEL, "<$file" or die $!;
    $file =~ s/Model_//; #remove string subset from string $file
    $file =~ s/\.txt$//;

    <MODEL>; #skip header
    while(<MODEL>){
        my @columns = split ("\t", $_);
        my $model_number = shift @columns;
        my $geneList = getGenesInModel($file, $model_number);

        my @CandSigma = getCandSigma($file, $model_number);
        my $C = $CandSigma[0];
        my $sigma = $CandSigma[1];

        my $file_model = $file."_".$model_number;

        unshift(@columns, $geneList);
        unshift(@columns, $sigma);
        unshift(@columns, $C);
        unshift(@columns, $file_model);
        
        $infoArray.push(@infoArray, \@columns);
        $model_ctr ++;
    }
    close MODEL;
}

my @sorted_infoArray = sort {abs($b->[14]) <=> abs($a->[14])} @infoArray;
#my @sorted_infoArray = @infoArray;

open OUTMODEL, ">sortedModels.tsv" or die $!;
print OUTMODEL"Set_of_parameters_modelNumber\tC_Value\tSigma_Value\tGenes\tMisclass\tMisclass_Error\tLogLoss\tLogLoss_Error\tGOF\tGOF_Error\tAccuracy0\tAccuracy1\tAccuracy0_err\tAccuracy1_err\tMCC\n";
my $ctr = 0;
my $outctr = 0;
my $length = @sorted_infoArray;
for ($outctr = 0; $outctr < $length; $outctr ++){
    for (my $i = 0; $i <= 14; $i++){
        if ($i == 14){
            print OUTMODEL "$sorted_infoArray[$outctr][$i]";
            print "$infoArray[$outctr][$i]\n";
        } else {
            print OUTMODEL "$sorted_infoArray[$outctr][$i]\t";
            print "$infoArray[$outctr][$i]\t";
        }
    }
}

close OUTMODEL;


print "row 1: $infoArray[3][0]";
print "\n$model_ctr models were processed\n";
print "Info array length: $length\n";

#Add the genes in the model to the amalgamated file
sub getGenesInModel
{
    my ($file, $model_number) = @_;
    my $genes;

    my $path_to_gene_file = "../$file/Model.txt";

    #Map genes to skip
    # here we grab the genes to skip (genes with no probes)
    my $skippedGenes = "../$file/Output.Skipped-genes.txt";
    open SKIPPEDGENES, "< $skippedGenes" or die $!;

    my @genesToSkip;

    while (<SKIPPEDGENES>) {
        chomp;
        @genesToSkip = split("\t", $_);
    }

    my %hash_genesToSkip = map {$_ => 1} @genesToSkip;

    close SKIPPEDGENES;

    ##################################

    open MODEL_FILE, "<$path_to_gene_file" or die $!;
    
    my $ctr = 0;
    while (<MODEL_FILE>){
        chomp;
        if ($ctr == $model_number){
            my @columns = split("\t", $_);
            shift(@columns); #remove C
            shift(@columns); #remove Sigma
            shift(@columns); #remove misclassfication
            #now only left with genes
            foreach my $gene (@columns){
                if(exists($hash_genesToSkip{$gene})) {
                    print "Delete: $_ \n";
                    next;
		        } #skip genes with no probes
                $genes = $genes.$gene.";";
            }
            last;
        }
        $ctr++;
    }
    close MODEL_FILE;


    $genes =~ s/\;$//; 
    return $genes;
}

#Add the C and Sigma values of the model to the amalgamated file
sub getCandSigma
{
    my ($file, $model_number) = @_;
    my $genes;
    my $C;
    my $sigma;

    my $path_to_gene_file = "../$file/Model.txt";
    open MODEL_FILE, "<$path_to_gene_file" or die $!;
    
    my $ctr = 0;
    while (<MODEL_FILE>){
        chomp;
        if ($ctr == $model_number){
            my @columns = split("\t", $_);
            $C = $columns[0];
            $sigma = $columns[1];
            last;
        }
        $ctr++;
    }
    close MODEL_FILE;

    return ($C, $sigma);
}
