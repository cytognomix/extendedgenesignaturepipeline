#!usr/bin/perl
#28-10-2019 MH: Soft coded step0, step1, and step2 genes and takes values from the command line

use warnings; use strict;

# this program will read MFA.tsv and identify which genes did not correlate to either factor ("0" genes)
# and which genes are correlating due to invariant CN (angles are exactly 0, 90 or 180)
# MH: Program chooses label SVM file based on file inputted in 

# MH: Takes step0, step1, and step2 degrees given in the command line. If degree == 0, skip no node (e.g. step2 == 0 means no node 2)
my $num_args = $#ARGV + 1;
if ($num_args != 4) {
    print "\nUsage : file.pl step0 step1 step2 svmFile\n";
    die('Step0, Step1, and Step2 threshold angles and SVM file must be specified' . "\n");
}
my ($step0, $step1, $step2, $labelFile) = @ARGV;

my $mindegree = 20; #not used

open MFA, "<MFA.tsv" or die $!;

#addding a output file for Jonatahn's Summarizing Program that contains the list of genes
#open OUTGENES, ">Genes.DONT-USE.txt" or die $!;

my %genestoskip;
my $count = 0;

while (<MFA>) {

	chomp;
	my @columns = split ("\t", $_);

	# example input
	# gene    copy number angle       expression angle        strongest       sign    steps
	# ABCB1   94.28633467944934       17.1445591589231        expression      positive        0
	next if ($_ =~ /^Gene/);	

	my $gene = $columns[0];
	#adding a print function to generate a gene file for Jonathan's Summarizing program
	#end of Jem's addition
	my $CN = $columns[1];
	my $GE = $columns[2];
	my $strongest = $columns[3];

	my $step = $columns[5];

	$genestoskip{$gene} = 1 unless ($strongest eq "expression");
	next unless ($strongest eq "expression");

	if ($step == 2) {
		if ((($GE > $step2) && ($GE < (180 - $step2))) || ($step2 == 0)) {
			# next if 2 step gene is greater than the minimum angle
			$genestoskip{$gene} = 1;
		}

	} elsif ($step == 1) {
		if ((($GE > $step1) && ($GE < 180 - ($step1))) || ($step1 == 0)) {
			# next if 1 step gene is greater than the minimum angle
			$genestoskip{$gene} = 1;

		}	


	} elsif ($step == 0) {
		if ((($GE > $step0) && ($GE < (180 - $step0))) || ($step0 == 0)) {
	        	# next if 0 step gene is greater than the minimum angle+10
			$genestoskip{$gene} = 1;
		}

	}


	# if there is no correlation
#	if ((($CN > $mindegree) && ($CN < (180 - $mindegree))) && (($GE > $mindegree) && ($GE < (180 - $mindegree)))) {


	#	$genestoskip{$gene} = 1;
	#	next;
	#}

	#print OUTGENES "$columns[0]", "\t";
	#if (($CN >= 90 - 0.0000000001) && ($CN <= 90 - 0.0000000001)) {
#		$genestoskip{$gene} = 1;
#	}
}

close MFA;

open SVM, "<./SVM/$labelFile" or die $!;

my %colstoskip;

open OUTLABEL, ">Label.txt" or die $!;
open OUTFEATURES, ">Features.Numerical-Matrix.txt" or die $!;
open OUTFEATWGENES, ">Features.With-Gene-Header.txt" or die $!;





while (<SVM>) {
	chomp;
	my @columns = split ("\t", $_);

	# in this part, we have to recognize the genes in the first loop, and skip printing their specific columns
	
	$count = 0;
	my $firstentryflag = 0;

	if ($_ =~ /^Cell Line/) {
		# it's the header, check for gene names here
		foreach(@columns) {
			my $header = $_;
			if (exists $genestoskip{$header}) {
				# this column number exists to be skipped
				$colstoskip{$count} = 1;

			} else {
				if ($count == 0) {
					#print "$header";
				} else {
					#print "\t$header";
				}
				if ($count > 2) {
					print OUTFEATWGENES "$header" if ($firstentryflag == 0); # no tab for first entry
					print OUTFEATWGENES "\t$header" if ($firstentryflag == 1);
					$firstentryflag = 1;
				}
	
			}
			$count++;
		}
		#print "\n";
		print OUTFEATWGENES "\n";
		
	} else {
		my $onflag = 0;
		foreach(@columns) {
			my $values = $_;
			if (exists $colstoskip{$count}) {
				# do nothing - thus, we skip the genes we don't want

			} else {
				 if ($count == 0) {
                                        #print "$values";
                                } else {
                                        #print "\t$values";

					if ($count == 2) {
						# its the label
						print OUTLABEL "$values\n";
					} elsif ($count > 2) {
						# print only the genes we want	
						print OUTFEATURES "$values" if ($onflag == 0); # two prints for printing (no tab for first entry, tab for the rest)
						print OUTFEATURES "\t$values" if ($onflag == 1);
                                                print OUTFEATWGENES "$values" if ($onflag == 0); # two prints for printing (no tab for first entry, tab for the rest)
                                                print OUTFEATWGENES "\t$values" if ($onflag == 1);

				
												
			
						$onflag = 1;
					}
                                }

			}
	

			$count++;
		}
		print OUTFEATURES "\n";
		print OUTFEATWGENES "\n";
		#print "\n";

	}

}

close SVM;


close OUTLABEL;
close OUTFEATURES;
close OUTFEATWGENES;


system ("head -1 Features.With-Gene-Header.txt > Genes.txt");
